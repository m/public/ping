import { Component, ComponentFactoryResolver, ElementRef, Input, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupService } from './popup.service';
import { MatButtonModule } from '@angular/material/button';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { PopupMapDirective } from './popup-map.directive';

@Component({
  selector: 'app-popup-map',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatSlideToggleModule, FormsModule, MatIconModule],
  templateUrl: './popup-map.component.html',
  styleUrls: ['./popup-map.component.scss']
})
export class PopupMapComponent implements OnInit {

  @ViewChild(PopupMapDirective, { static: true }) dynamicContainer: PopupMapDirective;
  data : any[] = [];
  
  descriptionAffichee : boolean = true;
  btndescription : boolean = false;

  constructor(public viewContainerRef: ViewContainerRef, private popupService : PopupService) { }
  
  
  ngOnInit(): void {
    // this.popupService.dataPopUp.subscribe((tmp : {feature: any, btnDescription : boolean, [key: string]: any}[] | {feature: any, btnDescription : boolean, [key: string]: any}) => {

    //   this.dynamicContainer.viewContainerRef.clear();
    //   this.data = [];

    //   if(tmp === undefined || tmp === null) return;      

    //   if(!Array.isArray(tmp)) this.data.push(tmp);
    //   else this.data = tmp;
            
    //   this.data.forEach(element => {
    //     this.btndescription = element.btnDescription;
    //     this.loadPopUp(element.feature.getProperties().typePopUp || element.feature.getProperties().type || '', element);
    //   });
        
    // });
  }

  loadPopUp(type: string, data: any) {
    const ref = this.dynamicContainer.viewContainerRef.createComponent(this.popupService.getComponent(type));
    ref.instance.data = data;
  }

  close(event: Event) {
    event.stopPropagation();
    this.popupService.hidePopup();
  }
}
