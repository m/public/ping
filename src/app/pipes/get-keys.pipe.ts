import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getKeys',
  standalone: true
})
export class GetKeysPipe implements PipeTransform {

  transform(value: unknown): string[] {
    return Object.keys(value);
  }

}
