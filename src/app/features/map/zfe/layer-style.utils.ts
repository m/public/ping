// layer-style.util.ts
import { Style, Fill, Stroke } from 'ol/style';
import { Feature } from 'ol';
import { MapComponent } from '@metromobilite/m-map';
import { MlayersCircleStyle, MlayersLineStyle, MlayersPlygonStyle } from '@metromobilite/m-map/m-layers';

export function getLayerStyle(
  feature: Feature,
  component: MapComponent,
  vehicleType: 'private' | 'heavy',
  lineStyle: MlayersLineStyle,
  polygonsStyle: MlayersPlygonStyle
): Style | null | Style[]{
  if (!feature.get('visible') || (feature.get('vehicle_type') !== vehicleType && feature.get('vehicle_type') !== 'global')) {
    return null;
  }

  const useDark = component.tiles
    .filter((config) => config.visible)
    .reduce((_, config) => config.layer.includes('dark'), true);

  const type = feature.getProperties()?.type;

  if (type === 'zfeaires') {
    return polygonsStyle.build({
      color: useDark ? '#FF2C2C45' : '#FF2C2C15',
      strokeColor: useDark ? '#FF2C2C' : '#FF2C2C',
      lineDash: [10, 10],
      width: 6,
    })
    // return new Style({
    //   stroke: new Stroke({
    //     color: useDark ? '#FF2C2C' : '#FF2C2C',
    //     lineDash: [10, 10],
    //     lineCap: 'square',
    //     width: 6,
    //   }),
    //   fill: new Fill({
    //     color: useDark ? '#FF2C2C45' : '#FF2C2C15',
    //   })
    // });
  } else if (type === 'zfevoies') {
    return lineStyle.build({
      width: 2,
      color: useDark ? 'rgba(230, 230, 230)' : 'rgba(100, 100, 100)',
    })
    // return new Style({
    //   stroke: new Stroke({
    //     color: useDark ? 'rgba(230, 230, 230)' : 'rgba(100, 100, 100)',
    //     width: 4,
    //     lineCap: 'round'
    //   })
    // });
  }

  return null;
}
