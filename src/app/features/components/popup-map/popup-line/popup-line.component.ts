import { Component, Input } from '@angular/core';
import { CommonModule, JsonPipe } from '@angular/common';
import { PopUpType } from '@features/map/map.model';
import { ApiService, CoreModule, Line, LinesService, RoutesTypeService, RouteType } from '@metromobilite/m-features/core';
import { Feature } from 'ol';
import { PopupMapComponent } from "../popup-map.component";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { FormsModule } from '@angular/forms';
import { PlanTCService } from '@pages/plan-tc/plan-tc.service';
import { LogoLigneComponent } from '@features/components/logo-ligne/logo-ligne.component';

@Component({
  selector: 'app-popup-line',
  standalone: true,
  imports: [CommonModule, JsonPipe, CoreModule, PopupMapComponent, MatSlideToggleModule, FormsModule, MatCheckboxModule, LogoLigneComponent],
  templateUrl: './popup-line.component.html',
  styleUrls: ['./popup-line.component.scss']
})
export class PopupLineComponent implements PopUpType {
  typePopUp: string = 'line';

  descriptionAffichee : boolean = false;

  @Input() data : any;
  lines : Line[] = [];
  selectedLinesId : string[] = [];

  constructor(private planTCService : PlanTCService) {}

  ngOnInit(): void {
    this.descriptionAffichee = this.data.data.lines.length < 4;
    this.data.data.lines.forEach(element => {
      this.lines.push(element.feature.getProperties());
    });
  }

  trackByFn(index: number, line: Line) {
		return line.id;
	}

  selected(line : Line){
    return this.planTCService.isSelected(line);
  }
}
