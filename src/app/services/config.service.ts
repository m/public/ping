import { BreakpointObserver, BreakpointState } from "@angular/cdk/layout";
import { Injectable } from "@angular/core";
import { ConfigService, FRONT_CONFIG } from "@metromobilite/m-features/core";
import { BehaviorSubject, filter, Observable, Subject, take } from "rxjs";
import { CurrentRouteStaticData, RouteService } from "./route.service";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";


@Injectable({
	providedIn: 'root'
})
export class ConfigPingService {

  private pingConfig : any
  private temporaryConfig : any = {};

  finalConfigObj : BehaviorSubject<any> = new BehaviorSubject({});
  finalConfig : any = {};

  isExternalConfig : boolean = false;

  isConfigOk : BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private configServiceFeature: ConfigService, private routeService: RouteService, private http: HttpClient, private router: Router) {


    this.router.events
    .pipe(filter(event => event instanceof NavigationEnd))
    .subscribe((event: NavigationEnd) => {

      if(this.routeService.currentRouteStaticData && this.routeService.currentRouteStaticData.queryParams && this.routeService.currentRouteStaticData.queryParams['config_url']) {
        this.isExternalConfig = true;
        this.loadExternalConfig(this.routeService.currentRouteStaticData.queryParams['config_url']);        
      } else {
        this.pingConfig = this.configServiceFeature.config[FRONT_CONFIG.PING];
        this.finalConfigObj.next(this.pingConfig);
        this.finalConfig = JSON.parse(JSON.stringify(this.pingConfig));
        this.isConfigOk.next(true);
      }


    });
	}

  loadExternalConfig(url : string) {
    this.http.get(url).subscribe(data => {
      this.pingConfig = data;
      this.finalConfig = data;
      this.finalConfigObj.next(data);
      this.isConfigOk.next(true);
    });
  }

  getFinalConfigObs(): Subject<any>{
    return this.finalConfigObj;
  }

  setTemporaryConfig(conf : any) {
    this.temporaryConfig = conf;
    if(Object.keys(conf).length > 0) this.finalConfig = conf;
    else this.finalConfig = this.pingConfig;
    this.finalConfigObj.next(this.finalConfig)
  }
    
}