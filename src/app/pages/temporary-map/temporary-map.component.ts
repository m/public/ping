import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemporaryMapSource } from '@features/map/temporary-maps/temporary-maps.source';
import { ConfigService, FRONT_CONFIG } from '@metromobilite/m-features/core';
import { MapClickedFeature, PingConfig } from '@features/map/map.model';
import { RouteService } from '@services/route.service';
import { MapService } from '@features/map/map.service';
import { combineLatest, debounceTime, Subject, takeUntil } from 'rxjs';
import { PopupService } from '@features/components/popup-map/popup.service';
import { MatIconModule } from '@angular/material/icon';
import { GlobalService } from '@services/global.service';
import { ConfigurationComponent } from "../configuration/configuration.component";
import { ConfigPingService } from '@services/config.service';

@Component({
  selector: 'app-temporary-map',
  standalone: true,
  imports: [CommonModule, MatIconModule, ConfigurationComponent],
  templateUrl: './temporary-map.component.html',
  styleUrls: ['./temporary-map.component.scss']
})
export class TemporaryMapComponent implements OnInit, OnDestroy {
  private unsubscriber: Subject<void> = new Subject();
  temporaryMapDescription: string | null = null;
  isMobile: boolean = false;
  shouldAnimate: boolean;

  currentConfig : any = {};

  constructor(private temporaryMapSource: TemporaryMapSource, private configService: ConfigPingService, private globalService: GlobalService,
    public routeService: RouteService, private mapService: MapService, private popupService: PopupService
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('visitedEmptyTemporaryMap')) {
      this.shouldAnimate = false;
    } else {
      this.shouldAnimate = true;
      localStorage.setItem('visitedEmptyTemporaryMap', 'true');
    }

    this.globalService.isMobile
    .pipe(takeUntil(this.unsubscriber))
    .subscribe(isMobileState => this.isMobile = isMobileState.matches);

    this.mapService.featureClicked$
      .pipe(takeUntil(this.unsubscriber))
      .subscribe((featureClicked: MapClickedFeature[] | null) => {
        this.popupService.hidePopup();    //cacher la popup éventuellement ouverte
        this.popupService.dataPopUp.next(null); //supprimer le contenu éventuel de la popup
        let data = featureClicked;
        if (featureClicked && featureClicked.length > 0) {
          featureClicked.every(element => {
            this.popupService.dataPopUp.next({ data, typeData: 'temporary-map', btnDescription: false });
            this.popupService.showPopUp(element.coordinate);
          });

          // this.selectFeature(featureClicked[0].feature);
        } else {
          // this.resetFeatureStyles();
        }
      });

    combineLatest(
      [this.configService.isConfigOk.pipe(takeUntil(this.unsubscriber)),
      this.configService.finalConfigObj.pipe(takeUntil(this.unsubscriber))]
    ).subscribe(([isOk, conf]) => {
      if(!isOk) return;
      
      if(conf.temporaryMaps) {
        const currentSlug = this.routeService.currentRouteStaticData.params.slug;
        const temporaryMapConfig = this.configService.finalConfig.temporaryMaps[currentSlug];      
  
        if(JSON.stringify(this.currentConfig) !== JSON.stringify(temporaryMapConfig) && temporaryMapConfig){        
          this.loadData(temporaryMapConfig, currentSlug);
        }
      }
    });

  };

  loadData(temporaryMapConfig, currentSlug){
    this.currentConfig = temporaryMapConfig;    
    this.temporaryMapSource.loadData(temporaryMapConfig, currentSlug)
      .pipe(takeUntil(this.unsubscriber))
      .subscribe(response => {
        if (response) {
          this.mapService.hideTemporaryLayer();
          this.mapService.showTemporaryLayer();
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
    this.mapService.hideTemporaryLayer();
  }
}
