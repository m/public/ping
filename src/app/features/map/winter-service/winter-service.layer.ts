import { Injectable } from '@angular/core';
import { LayerInputConfig, MapLayer, MapLayerBase, MapComponent } from '@metromobilite/m-map';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { WinterServiceReportSource } from './winter-service.source';
import { LineStyleConfig, MlayersLineStyle } from '@metromobilite/m-map/m-layers';
import { darkenColor } from '../utils/colors.utils';

@Injectable({ providedIn: 'root' })
@MapLayer('m-ping:winter-service')
export class WinterServiceReportLayer extends MapLayerBase {

  conditionsCirculationColorMap: { [key: string]: string } = {
    C1: '#66CC33',
    C2: '#FF6600',
    C3: '#ff0000',
    C4: '#000000',
  };

  zOffsetMap: { [key: string]: number } = {
    C1: 1,
    C2: 2,
    C3: 3,
    C4: 4,
  };

  constructor(
    public source: WinterServiceReportSource,
    private lineStyle: MlayersLineStyle
  ) { super(); }

  buildLayer(component: MapComponent, config: string | LayerInputConfig): BaseLayer {
    const layer = new VectorLayer({
      source: this.source.getSource() as VectorSource,
      style: (feature, resolution) => {

        const useDark = component.tiles.filter(config => config.visible).reduce((_, config) => config.layer.includes('dark'), true);

        const conditions_circulation: string = feature.get('conditions_circulation') || null;
        const isHovered = feature.get('hovered') || false;
        const someFeatureIsHovered = (this.source.getSource() as VectorSource).getFeatures().some(f => f.get('hovered'));
        const isSelected = feature.get('selected') || false;
        const someFeatureIsSelected = this.source.getSource().getFeatures().some(f => f.get('selected'));
        
        if (!conditions_circulation) {
          return [];
        }

        const baseColor = this.conditionsCirculationColorMap[conditions_circulation];

        const lineStyleConfig: LineStyleConfig = {
          color: baseColor,
          backgroundColor: typeof config === 'object' && config.backgroundColor || '#000',
          noBorder: false,
          width: 5,
          zOffset: this.zOffsetMap[conditions_circulation]
        };

        if ((someFeatureIsHovered && !isHovered) || (someFeatureIsSelected && !isSelected) ) {
          lineStyleConfig.color = 'rgba(150, 150, 150, 0.7)';
          if(!useDark) lineStyleConfig.color = 'rgba(240, 240, 240, 0.7)';
          lineStyleConfig.width = 4;
        }

        if (isHovered || isSelected) {
          lineStyleConfig.color = baseColor;
          lineStyleConfig.width = 6;
        }

        return this.lineStyle.build(lineStyleConfig);
      }
    });
    return layer;
  }
}
