import { Component, forwardRef, OnDestroy, OnInit } from '@angular/core';
import { CommonModule, NgSwitch, NgSwitchCase } from '@angular/common';
import { FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule, UntypedFormControl } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { Subject, takeUntil } from 'rxjs';
import { RouteService } from 'src/app/services/route.service';
import { ThemeToggleService } from '@services/theme-toggle.service';

@Component({
  selector: 'm-ping-theme-toggle',
  templateUrl: './theme-toggle.component.html',
  styleUrls: ['./theme-toggle.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ThemeToggleComponent),
      multi: true
    }
  ],
  standalone: true,
  imports: [FormsModule, CommonModule, ReactiveFormsModule, MatIconModule, NgSwitch, NgSwitchCase]
})


export class ThemeToggleComponent implements OnInit, OnDestroy {
  control = new UntypedFormControl();
  selectedLabel = '';


  private unsubscriber = new Subject<void>();

  constructor(private themeService: ThemeToggleService, private routeService: RouteService) {
    if (localStorage.getItem(ThemeToggleService.LS_KEY) !== null) {
      this.writeValue(localStorage.getItem(ThemeToggleService.LS_KEY));
    }
    this.control.valueChanges.pipe(takeUntil(this.unsubscriber)).subscribe({
      next: value => {
        this.themeService.toggleTheme(value);
      }
    });
  }

  ngOnInit(): void {
    const routeTheme = this.routeService.currentRouteStaticData.queryParams['theme'];

    if (routeTheme) {
      this.writeValue(routeTheme);
    }
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  writeValue(value: string): void {
    if (value) {
      this.control.setValue(value, { emitEvent: false });
    }
  }
}
