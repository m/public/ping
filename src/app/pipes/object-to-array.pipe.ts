import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'objectToArray',
	standalone: true
})
export class ObjectToArrayPipe implements PipeTransform {

	transform(value: any, args: string[]): any[] {
		if (!value) return [] as Object[];
		let prop: string = args[0];
		let keyArr: any[] = Object.keys(value), dataArr: any[] = [], keyName = prop;

		keyArr.forEach((key: any) => {
			if (typeof (value[key]) === 'object') {
				value[key][keyName] = key;
				dataArr.push(value[key]);
			}

		});		
		return dataArr;
	}

}
