import { Component, Input, OnInit } from '@angular/core';
import { CommonModule, NgFor } from '@angular/common';
import { PopUpType } from '@features/map/map.model';
import { CoreModule, Line } from '@metromobilite/m-features/core';
import { PopupLineComponent } from "../popup-line/popup-line.component";
import { PopupMapComponent } from "../popup-map.component";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule } from '@angular/forms';
import { LogoLigneComponent } from '@features/components/logo-ligne/logo-ligne.component';

@Component({
  selector: 'app-popup-cluster',
  standalone: true,
  imports: [CommonModule, CoreModule, NgFor, PopupLineComponent, PopupMapComponent, MatSlideToggleModule, FormsModule, LogoLigneComponent],
  templateUrl: './popup-cluster.component.html',
  styleUrls: ['./popup-cluster.component.scss']
})
export class PopupClusterComponent implements OnInit, PopUpType {
  typePopUp: string = 'clusters';

  @Input() data : any;
  descriptionAffichee : boolean = true;
  lines : Line[] = [];

  ngOnInit(): void {       
    this.data.data.lines.forEach(element => {
      if(element == null) return;
      this.lines.push(element.getProperties());
    });    
  }
}
