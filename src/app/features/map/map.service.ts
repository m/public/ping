import { Inject, Injectable } from '@angular/core';
import { THEMES } from '@metromobilite/m-features/core';
import { LayerInputConfig, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE, MapComponent } from '@metromobilite/m-map';
import { MapFiltersService, MLayerHelpersService, MLayersCyclePathsSource, MLayersLinesSource, MLayersRoadSectionSource, PoiLayerHelper } from '@metromobilite/m-map/m-layers';
import { Coordinate } from 'ol/coordinate';
import { Extent } from 'ol/extent';
import Feature, { FeatureLike } from 'ol/Feature';
import Point from 'ol/geom/Point';
import { toLonLat, transform } from 'ol/proj';
import { getDistance } from 'ol/sphere';
import { ConnectableObservable, Observable, Subject, Subscriber } from 'rxjs';
import { publish, shareReplay } from 'rxjs/operators';
import { MapWrapperComponent } from './map-wrapper/map-wrapper.component';
import { MapClickedFeature, MapPositionOptions } from './map.model';

@Injectable({
    providedIn: 'root',
})
export class MapService {
    readonly HOME_MAP_STATE_KEY = '/accueil/bus-tram';
    initialized = false;
    //theme: THEMES;
    initMap = new Subject<boolean>();
    mapComponent: MapWrapperComponent;
    selectedFeature?: Feature;
    featureClicked$: Subject<MapClickedFeature[]> = new Subject();
    featureHovered$: Subject<MapClickedFeature> = new Subject();
    noticeMap$: Subject<string> = new Subject();

    constructor(
        @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string, @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string) {
        this.initMap.subscribe((initialized) => {
            this.initialized = initialized;
        });
    }

    emitFeatureClicked(features: MapClickedFeature[]): void {
        this.featureClicked$.next(features);
    }
    emitFeatureHovered(feature: MapClickedFeature): void {
        this.featureHovered$.next(feature);
    }

    /**
     * Should be used by the map-wrapper only.
     */
    setMap(mapComponent: MapWrapperComponent): void {
        this.mapComponent = mapComponent;
    }


    fitView(extent: Extent, offsetBottom = 0, duration = 200) {
        const padding = 32;
        const fit$ = new Observable((obs) => {
            this.mapComponent.view.fit(extent, {
                padding: [padding + 16, padding, padding + offsetBottom, padding],
                duration,
                callback: (complete) => {
                    obs.next(complete);
                    obs.complete();
                },
            });
        }).pipe(shareReplay(1));
        publish()(fit$).connect();
        return fit$;
    }

    getExtent(){
        return this.mapComponent.view.calculateExtent()
    }

    setCenter(center : any){
        this.mapComponent.view.animate({
            center: center,
            duration: 200
        });
    }

    /**
     * Modifie le zoom et/ou le centre de la carte
     * @param data {zoom: number, lonlat: Coordinate, duration: number}
     */
    setMapPosition(data: MapPositionOptions): Observable<boolean> {
        const forceZoom: number = data.zoom;
        const forceLonLat: number[] = data.center;

        const position$: Observable<boolean> = new Observable<boolean>((obs: Subscriber<boolean>): void => {
            this.mapComponent.view.animate({
                center: forceLonLat || null,
                zoom: forceZoom || this.mapComponent.view.getZoom(),
                duration: data.duration || 200
            }, (complete: boolean): void => {
                obs.next(complete);
                obs.complete();
            });
        }).pipe(shareReplay(1));
        const published: ConnectableObservable<any> = publish()(position$);
        published.connect();
        return position$;
    }

    reloadAllSources(){
        this.mapComponent.reloadAllSources();
    }

    showZfePrivateVehiclesLayer() {
       this.mapComponent.showZfePrivateVehiclesLayer();
    }
    
    showZfeHeavyVehiclesLayer() {
        this.mapComponent.showZfeHeavyVehiclesLayer();
    }

    showZFEAireLayers(): void {
        this.mapComponent.showZFEAireLayers();
    }

    hideZfePrivateVehiclesLayer() {
        this.mapComponent.hideZfePrivateVehiclesLayer();
     }
     
     hideZfeHeavyVehiclesLayer() {
         this.mapComponent.hideZfeHeavyVehiclesLayer();
     }
    
    hideZFEAireLayers(): void {
        this.mapComponent.hideZFEAireLayers();
    }

    showWinterServiceLayer(): void {
        this.mapComponent.showWinterServiceLayer();
    }

    hideWinterServiceLayer(): void {
        this.mapComponent.hideWinterServiceLayer();
    }

    showPlanTCServiceLayer(): void {
        this.mapComponent.showPlanTCServiceLayer();
    }

    hidePlanTCServiceLayer(): void {
        this.mapComponent.hidePlanTCServiceLayer();
    }

    showClustersServiceLayer(): void {
        this.mapComponent.showClustersServiceLayer();
    }

    hideClustersServiceLayer(): void {
        this.mapComponent.hideClustersServiceLayer();
    }

    showTemporaryLayer(): void {
        this.mapComponent.showTemporaryLayer();
    }

    hideTemporaryLayer(): void {
        this.mapComponent.hideTemporaryLayer();
    }
}
