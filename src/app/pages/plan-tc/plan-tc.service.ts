import { Inject, Injectable, QueryList, Type } from '@angular/core';
import { MatExpansionPanel } from '@angular/material/expansion';
import { ActivatedRoute, Router } from '@angular/router';
import { PopUpType } from '@features/map/map.model';
import { MapService } from '@features/map/map.service';
import { PlanTCServiceSource } from '@features/map/plan-tc/planTC-service.source';
import { Line, RouteType } from '@metromobilite/m-features/core';
import { ApiMService } from '@services/api-m.service';
import { GlobalService } from '@services/global.service';
import { Coordinate } from 'ol/coordinate';
import { intersects } from 'ol/extent';
import Feature from 'ol/Feature';
import { BehaviorSubject, Subject, takeUntil } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlanTCService {


  private unsubscriber: Subject<any> = new Subject();
  mapClustersIdByLineId : Map<string, string[]> = new Map();
  routeTypes: RouteType[];
  panels: QueryList<MatExpansionPanel>
  searchTerm: string = '';

  constructor(private planTCServiceSource : PlanTCServiceSource, private mapService: MapService, private apiMService : ApiMService, 
    private globalService : GlobalService, private routerActive: ActivatedRoute, private router: Router) {


  }

  isSelected(line : Line) {
    return this.planTCServiceSource.source.getFeatureById(line.id.replace(':', '_'))?.get('selected') || false
  }

  selectLineById(id: string, onlyAdd: boolean, setUrl: boolean, activePanel : boolean): Feature {       
    
    //afficher une ligne. Si on ne trouve pas de feature avec cet id, alors on stop la méthode
    const isViewLine = this.viewLineById(id, onlyAdd)
    
    if(isViewLine == null) return null;       

    if(!this.searchTerm && activePanel) this.activePanel(id);

    //si on ajoute une nouvelle ligne alors
    if(isViewLine){
      //on cherche tous les clusters de cette ligne
      this.apiMService.getClustersOfRoute(id).pipe(takeUntil(this.unsubscriber)).subscribe((clusters) => {            
        clusters.forEach(oneCluster => {     
          //on ajoute tous les arrêts de la ligne dans une map
          this.mapClustersIdByLineId.set(id, (this.mapClustersIdByLineId.get(id) || []).concat(oneCluster.code));
        });
        //afficher toutes les lignes choisies
        this.showClusters();
      });
      if(setUrl) this.setLignesInUrl();

      return this.planTCServiceSource.source.getFeatureById(id.replace(':', '_'));
    }
    this.mapClustersIdByLineId.delete(id);
    this.disableClusters();
    this.showClusters();

    if(setUrl) this.setLignesInUrl();

    return null;
    
  }

  /**
 * Handles the selection and visibility of lines on the map based on the provided lineId.
 *
 * @param lineId - The unique identifier of the line to be selected or deselected.
 * @param onlyAdd - A flag indicating whether to only add the line to the selection (default is false).
 *
 * @returns {void}
 */
  viewLineById(lineId: string, onlyAdd: boolean = false): boolean | null{    
    const selectedFeature = this.planTCServiceSource.source.getFeatureById(lineId.replace(':', '_'));

    //par exemple, les navette
    if(typeof selectedFeature === 'undefined' || selectedFeature == null) return null;

    const mapExtent = this.mapService.getExtent();  //récupérer l'extent de la view de la map
    const geometryExtent = selectedFeature.getGeometry().getExtent();  //récupérer l'extent de la géométrie courante
    if(!intersects(mapExtent, geometryExtent)) { // si la géométrie courante n'est pas du tout dans le view de la map alors      
      const center = [ //récupérer le centre de la géométrie
        (geometryExtent[0] + geometryExtent[2]) / 2,
        (geometryExtent[1] + geometryExtent[3]) / 2,
      ];
      this.mapService.setCenter(center); //aller au centre de la map
    }
    

    //si on veut seulement selectionner une nouvelle ligne sans déselectionner la ligne si elle est déjà selectionnée
    if(onlyAdd) {
      selectedFeature.set('selected', true);
      selectedFeature.set('visible', true);
      return true;
    } else {
      //inverser la selection de la ligne
      const select = !selectedFeature.get('selected')
      selectedFeature.set('selected', select);
      return select;
    }
  }

  activePanel(idFeature){
    const properties = this.planTCServiceSource.source.getFeatureById(idFeature.replace(':', '_')).getProperties();
    let indexPanel = -1;
    this.routeTypes.every(route => {
      
      if(route.isPlanTC) indexPanel++;
      if((typeof route.type === 'string' && route.type === properties.id.split(':')[0]) || route.type.includes(properties.id.split(':')[0])){        
        this.panels.get(indexPanel).open();
        this.panels.get(indexPanel)._body.nativeElement.focus();
        this.panels.get(indexPanel)._body.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
        return false;
      }
      return true;
    });
  }

  setLignesInUrl() {    
    let queryParams : string = '';
    this.getSelectedLines().forEach(element => {
      if(queryParams !== '') queryParams += ',';
      queryParams += element;
    });

    this.router.navigate([], {
      relativeTo: this.routerActive,
      queryParams: { lignes: queryParams },
      replaceUrl: true
    });

    this.globalService.nbFeatureSelected.next(queryParams.split(',').length);
    this.globalService.actionPanel.next(true);    
  }

  getSelectedLines() {
    let idSelectedLines : string[] = [];
    this.planTCServiceSource.source.getFeatures().forEach(feature => {
      if(feature.getProperties().selected) idSelectedLines.push(feature.getProperties().id);
    });

    return idSelectedLines;
  }

    //desactiver les clusters
    disableClusters(){
      this.planTCServiceSource.sourcePOI.getFeatures().forEach((feature) => {
        feature.set('visibleMap', false);
        feature.set('selected', false);
      });
    }
  
    //desactiver toutes les lignes selectionnées
    disableLines(){
      this.planTCServiceSource.source.getFeatures().forEach((feature) => {
        feature.set('selected', false);
      });
      this.disableClusters();
    }
  
    showClusters(){    
      this.mapClustersIdByLineId.forEach((value: string[], key: string) => {      
        value.forEach(code => {        
          this.planTCServiceSource.sourcePOI.getFeatureById(code).set('visibleMap', true);
        });
      });
    }

}
