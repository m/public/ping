import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupMapComponent } from "../popup-map.component";
import { Feature } from 'ol';
import { MatDividerModule } from '@angular/material/divider';
import { GetKeysPipe } from '@pipes/get-keys.pipe';
import { PopUpType } from '@features/map/map.model';

@Component({
  selector: 'app-popup-temporary-map',
  standalone: true,
  imports: [CommonModule, PopupMapComponent, GetKeysPipe, MatDividerModule],
  templateUrl: './popup-temporary-map.component.html',
  styleUrls: ['./popup-temporary-map.component.scss']
})
export class PopupTemporaryMapComponent implements PopUpType {
    typePopUp: string = 'temporary-map';

    featuresInformations: Feature[] = [];

    @Input() data : any;

    ngOnInit(): void {
      this.data.data.forEach(feature => {
        this.featuresInformations.push({...feature.feature.get('meta')});
      });
    }
}
