export const WINTER_SERVICE_CATEGORIES = {
    'Météo': [
        { key: 't_air', label: 'Température de l\'air' },
        { key: 't_sol', label: 'Température du sol' },
        { key: 'brouillard', label: 'Brouillard' },
        { key: 'brouillard_givrant', label: 'Brouillard givrant' },
        { key: 'distance_visibilite', label: 'Distance de visibilité' }
    ],
    'État de la chaussée': [
        { key: 'chaussee_congeres', label: 'Chaussée avec congères' },
        { key: 'chaussee_enneigee', label: 'Chaussée enneigée' },
        { key: 'chaussee_humide', label: 'Chaussée humide' },
        { key: 'chaussee_mouillee', label: 'Chaussée mouillée' },
        { key: 'chaussee_seche', label: 'Chaussée sèche' },
        { key: 'chaussee_verglas_generalise', label: 'Chaussée avec verglas généralisé' },
        { key: 'chaussee_verglas_localise', label: 'Chaussée avec verglas localisé' }
    ],
    'Conditions de circulation': [
        { key: 'conditions_circulation', label: 'Conditions de circulation' }
    ],
    'Traitement': [
        { key: 'traitement_en_cours', label: 'Traitement en cours' },
        { key: 'traitement_termine', label: 'Traitement terminé' }
    ],
    'Équipements spéciaux': [
        { key: 'eqpt_conseille_sup75', label: 'Équipement conseillé pour > 7,5 tonnes' },
        { key: 'eqpt_conseille_tous', label: 'Équipement conseillé pour tous' },
        { key: 'eqpt_obligatoire_sup75', label: 'Équipement obligatoire pour > 7,5 tonnes' },
        { key: 'eqpt_obligatoire_tous', label: 'Équipement obligatoire pour tous' }
    ]
};

// Map for 'conditions_circulation' values to their human-readable labels
export const CirculationConditionsMap = {
  c1: 'Circulation normale',
  c2: 'Circulation délicate',
  c3: 'Circulation difficile',
  c4: 'Circulation impossible'
};


export const LimitePluieNeige = {
    limite_pluie_neige_chartreuse: "Chartreuse",
    limite_pluie_neige_vercors: "Vercors",
    limite_pluie_neige_belledonne: "Belledonne"
}
