import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE, MapSourceBase } from '@metromobilite/m-map';
import { GeometryService } from '@metromobilite/m-map/m-layers';
import { Feature } from 'ol';
import Geometry from 'ol/geom/Geometry';
import Source from 'ol/source/Source';
import GeoJSON from 'ol/format/GeoJSON';
import VectorSource from 'ol/source/Vector';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, startWith, takeUntil } from 'rxjs/operators';
import { LinesService } from '@metromobilite/m-features/core';
import { Point } from 'ol/geom';
import { transform } from 'ol/proj';

@Injectable({ providedIn: 'root' })
export class PlanTCServiceSource extends MapSourceBase {

  source: VectorSource;
  sourcePOI: VectorSource;
  private geoJSON = new GeoJSON();

  constructor(
    private http: HttpClient,
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
    private linesService: LinesService,
    private geometryService : GeometryService
  ) {
    super();
    this.source = new VectorSource({});
    this.sourcePOI = new VectorSource({});
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  /**
   * @internal
   */
  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  getSource(): VectorSource {
    return this.source as VectorSource;
  }

  getSourceClusters(): VectorSource {
    return this.sourcePOI as VectorSource;
  }

  refresh(): void {
    this.source.refresh();
    this.sourcePOI.refresh();
  }
  
  changed(): void {
    this.source.changed();
    this.sourcePOI.changed();    
  }

  getData(routesTypes : any): Observable<VectorSource> {
    return this.http.get<any>('@domain/@api/lines/poly?types=ligne').pipe(
      map((response : any) => {       
        
          response.features.forEach(f => {       

            if(typeof f.properties?.id == 'undefined' || typeof this.linesService.linesAsMap == 'undefined' || typeof this.linesService.linesAsMap[f.properties.id.replace('_', ':')] == 'undefined') return;
            const line = this.linesService.linesAsMap[f.properties.id.replace('_', ':')];
            
            const geometry = this.geometryService.getGeometry(f.properties.shape);
            delete f.properties.shape;

            let resolution = -1;
            let routeTypeSave : any = {};
            let isPlanTC = true;
            routesTypes.forEach(routeType => {
              if(typeof routeType.type === 'string' && routeType.type === f.properties.id.split('_')[0]) {
                routeTypeSave = routeType;
                return;
              }
              if(typeof routeType.type === 'object' && routeType.type.includes(f.properties.id.split('_')[0])) {
                routeTypeSave = routeType;
                return;
              }
            });
            if(Object.keys(routeTypeSave).length > 0) {
              if(typeof routeTypeSave.isPlanTC !== 'undefined') isPlanTC = routeTypeSave.isPlanTC;
              routeTypeSave.linesbloc.forEach(lineBloc => {
                if(lineBloc.type === line.type) {
                  resolution = lineBloc.resolutionMap || -1;
                  if(typeof lineBloc.isPlanTC !== 'undefined') isPlanTC = lineBloc.isPlanTC;
                  return;
                }
              });
            }
            
            if(!isPlanTC) return ;

            const feature = new Feature({
              typePopUp: 'line',
              ...line,
              resolution: resolution,
              geometry,
            });
            feature.setId(f.properties.id);

            feature.set('visible', isPlanTC);

            this.source.addFeature(feature);

          });
          
          return this.source;
      }),
      catchError(err => {
          console.error('Error fetching data', err);
          return of(undefined);
      })
    );
  }

  updateVisible(config : {[type : string]: boolean}) {
    this.source.forEachFeature(feature => {
      if(typeof config[feature.getProperties().type] === 'undefined' || !config[feature.getProperties().type]) {
        feature.set('visible', false);
        feature.set('selected', false);
      } else feature.set('visible', true);
    });   
  }

  getClusters(): Observable<VectorSource> {
    return this.http.get<any>('@domain/@api/points/json?types=clusters').pipe(
      map((response : any) => {
          response.features.forEach(f => {
            const geometry = new Point(transform(f.geometry.coordinates, this.dataProjection, this.featureProjection));

            const feature = new Feature({
              ...f.properties,
              geometry,
          });
          feature.setId(f.properties.code);

          feature.set('visibleMap', false);

          this.sourcePOI.addFeature(feature);

          });
          
          return this.sourcePOI;
      }),
      catchError(err => {
          console.error('Error fetching data', err);
          return of(undefined);
      })
    );
  }

}
