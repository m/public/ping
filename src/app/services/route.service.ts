import { Injectable } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { THEMES } from "@metromobilite/m-features/core";
import { MatomoTracker } from "ngx-matomo";
import { BehaviorSubject } from "rxjs";
import { filter, map, mergeMap, tap } from 'rxjs/operators';
import { ThemeToggleService } from "./theme-toggle.service";

export interface CurrentRouteStaticData {
  slug?: string;
  data?: any;
  params?: any;
  queryParams?: any;
  routePingConfig?: any;
  configCategory?: any;
}


@Injectable({
  providedIn: 'root'
})
export class RouteService {
  currentRouteStaticData: CurrentRouteStaticData = {
    data: {},
    params: {},
    queryParams: {},
    routePingConfig: {},
    configCategory: {},
    slug: ''
  }

  currentRouteStaticData$ = new BehaviorSubject<CurrentRouteStaticData>({});
  constructor(private router: Router, private activatedRoute: ActivatedRoute, public titleService: Title, private matomoTracker: MatomoTracker, private themeService: ThemeToggleService) {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map(route => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          this.currentRouteStaticData = {
            data: {},
            params: {},
            queryParams: {},
          };
          return route;
        }),
        filter(route => route.outlet === 'primary'),
        tap(route => {
          if (this.router.getCurrentNavigation()?.extras.state) {
            const navigation = this.router.getCurrentNavigation();
            this.currentRouteStaticData.configCategory = navigation.extras.state["configCategory"];
          }
          route.paramMap.subscribe(params => {
            params.keys.forEach(key => this.currentRouteStaticData.params[key] = params.get(key));
          });
          route.queryParamMap.subscribe(queryParams => {
            queryParams.keys.forEach(key => {
              if (key === 'theme') {
                this.themeService.toggleTheme(queryParams.get(key) as THEMES);
              }
              this.currentRouteStaticData.queryParams[key] = queryParams.get(key)
            });
          });
          const url = this.router.url;
          const slug = url.substring(url.lastIndexOf('/') + 1);
          this.currentRouteStaticData.slug = slug;
        }),
        mergeMap(route => route.data),
      )
      .subscribe((data: any) => {
        this.trackPageView(data.title);

        this.currentRouteStaticData.data = data;
        this.emitNewRouteConfiguration();
      });
  }

  trackPageView(title: string) {
    this.matomoTracker.setCustomUrl(this.router.url);
    this.matomoTracker.trackPageView(title);
  }

  emitNewRouteConfiguration() {
    this.currentRouteStaticData$.next(this.currentRouteStaticData);
  }

  // Utility function to get the current slug synchronously (to dynamically affect titles)
  getCurrentSlug(): string {
    const url = this.router.url;
    return url.substring(url.lastIndexOf('/') + 1);
  }
}
