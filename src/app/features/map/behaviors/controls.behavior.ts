import { Injectable } from '@angular/core';
import { AfterMapInit, MapBehavior, MapComponent } from '@metromobilite/m-map';


@Injectable({ providedIn: 'root' })
@MapBehavior('app:remove-default-controls')
export class AppMapControls implements AfterMapInit {

	onAfterMapInit(component: MapComponent): void {
		// Remove default controls.
		// component.map.getControls().clear();
		// See map-wrapper component for custom controls.
	}

}
