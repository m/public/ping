// Based on this: https://wiki.openstreetmap.org/wiki/Key:opening_hours

export function formatOpeningHours(hours: string): string {
    return hours
        .replace(/Mo/g, 'Lundi')
        .replace(/Tu/g, 'Mardi')
        .replace(/We/g, 'Mercredi')
        .replace(/Th/g, 'Jeudi')
        .replace(/Fr/g, 'Vendredi')
        .replace(/Sa/g, 'Samedi')
        .replace(/Su/g, 'Dimanche')
        .replace(/PH off/g, 'sauf jours fériés')
        .replace(/PH/g, 'jours fériés inclus')
        .replace(/SH off/g, 'sauf vacances scolaires')
        .replace(/SH/g, 'vacances scolaires incluses')
        .replace(/;/g, ' - ');
}
