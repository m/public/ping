import { BreakpointObserver, BreakpointState } from "@angular/cdk/layout";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";


@Injectable({
	providedIn: 'root'
})
export class GlobalService {

    isMobile = new Observable<BreakpointState>();
    actionPanel : BehaviorSubject<boolean> = new BehaviorSubject(false);
    nbFeatureSelected : BehaviorSubject<number> = new BehaviorSubject(0);

    constructor(private breakpointObserver: BreakpointObserver) {
		this.isMobile = this.breakpointObserver.observe(['(max-width: 1023px)']);
	}
    
}