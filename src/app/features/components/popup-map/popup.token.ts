import { InjectionToken } from "@angular/core";
import { PopUpType } from "@features/map/map.model";

export const POPUP_TYPE = new InjectionToken<PopUpType>('APP_POPUP_TYPE')