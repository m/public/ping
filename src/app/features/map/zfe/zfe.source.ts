import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { MapSourceBase, MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE } from '@metromobilite/m-map';
import { Feature } from 'ol';
import Geometry from 'ol/geom/Geometry';
import VectorSource from 'ol/source/Vector';
import GeoJSON from 'ol/format/GeoJSON';
import LineString from 'ol/geom/LineString';
import MultiLineString from 'ol/geom/MultiLineString';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ZFESource extends MapSourceBase {

    private dataFetched = false;
    source = new VectorSource();
    private geoJSON = new GeoJSON();

    constructor(
        private http: HttpClient,
        @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
        @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
    ) {
        super();
    }

    push(...features: Feature<Geometry>[]): void {
        this.push(...features);
    }

    remove(feature: Feature<Geometry>): void {
        this.remove(feature);
    }

    find(id: string): Feature<Geometry> | null {
        return this.find(id);
    }

    getSource(): VectorSource {
        return this.source as VectorSource;
    }

    getData(): Observable<boolean> {
        if (this.dataFetched) {
            // If data has already been fetched, return an observable of true
            return of(true);
        }

        // Fetch both areas and roads data
        const airesRequest = this.http.get<any>('@domain/@api/polygons/json?types=zfeaires');
        const voiesRequest = this.http.get<any>('@domain/@api/lines/json?types=zfevoies');

        return forkJoin([airesRequest, voiesRequest]).pipe(
            map(([airesResponse, voiesResponse]) => {
                this.processAiresData(airesResponse, new Date());
                this.processVoiesData(voiesResponse);
                this.dataFetched = true;
                return true;
            }),
            catchError(err => {
                console.error('Error fetching ZFE data', err);
                return of(false);
            })
        );
    }

    private processAiresData(response: any, currentDate: Date) {
        response.features.forEach(f => {
            const endDate = f.properties.date_fin ? new Date(f.properties.date_fin) : null;

            // Skip features that are entirely in the past
            if (endDate && endDate < currentDate) {
                return;
            }

            const geometry = this.geoJSON.readGeometry(f.geometry, {
                dataProjection: this.dataProjection,
                featureProjection: this.featureProjection
            });

            const feature = new Feature({
                ...f.properties,
                geometry,
            });
            feature.setId(f.properties.id);
            feature.set('vehicle_type', this.determineFeatureType(f.properties));
            feature.set('visible', true);
            this.source.addFeature(feature);
        });
    }

    private processVoiesData(response: any) {
        response.features.forEach(f => {
            const geometry = new LineString(f.geometry.coordinates).transform(
                this.dataProjection,
                this.featureProjection
            );

            const feature = new Feature({
                ...f.properties,
                geometry,
            });
            feature.setId(f.properties.id);
            feature.set('vehicle_type', this.determineFeatureType(f.properties));
            feature.set('visible', true);

            this.source.addFeature(feature);
        });
    }

    isFeatureInPeriod(feature: Feature, date: Date): boolean {
        const startDate = new Date(feature.getProperties().date_debut);
        const endDate = feature.getProperties().date_fin ? new Date(feature.getProperties().date_fin) : null;
        return endDate ? startDate <= date && date <= endDate : startDate <= date;
    }

    filterFeaturesByPeriod(date: Date, vehicleType: 'private' | 'heavy'): void {
        this.source.getFeatures().forEach(feature => {
            const type = feature.get('type');
            if (type === 'zfeaires') {
                const isValidType = feature.get('vehicle_type') === vehicleType;
                const isInPeriod = this.isFeatureInPeriod(feature, date);
                feature.set('visible', isValidType && isInPeriod);
            }
        });
        this.source.changed();
    }

    private determineFeatureType(properties: any): 'private' | 'heavy' | 'global' {
        if (!properties.vul_critair && !properties.pl_critair
            && !properties.vp_critair && !properties.deux_rm_critair) {
            return 'global';
        }
        if ((properties.type === 'zfevoies' && (!properties.vul_critair || !properties.pl_critair)) ||
            (properties.type === 'zfeaires' && (properties.vul_critair || properties.pl_critair))
        ) return 'heavy';

        return 'private';
    }

}
