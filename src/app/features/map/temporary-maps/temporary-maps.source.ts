import { Inject, Injectable } from '@angular/core';
import { PingDataType, PingTemporaryMap } from '../map.model';
import { catchError, forkJoin, map, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import VectorSource from 'ol/source/Vector';
import GeoJSON from 'ol/format/GeoJSON';
import Feature from 'ol/Feature';
import Polyline from 'ol/format/Polyline';
import { Point, LineString, MultiLineString, Polygon, MultiPolygon, Geometry } from 'ol/geom';
import { MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE, MapSourceBase } from '@metromobilite/m-map';

@Injectable({ providedIn: 'root' })
export class TemporaryMapSource extends MapSourceBase {
  source = new VectorSource({});
  private geoJSON = new GeoJSON();
  private polyline = new Polyline();
  currentTemporaryMapSlug: string;

  constructor(private http: HttpClient, @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string) {
    super();
  }


  loadData(temporaryMapConfig: PingTemporaryMap, temporaryMapSlug: string): Observable<boolean> {
    this.currentTemporaryMapSlug = temporaryMapSlug;
    const requests = this.prepareRequests(temporaryMapConfig.dataTypes);
    return forkJoin(requests).pipe(
      map((results) => results.every((result) => result)),
      catchError((error) => {
        console.error('Error loading temporary map data:', error);
        return of(false);
      })
    );
  }


  prepareRequests(dataTypes: PingDataType[]): Observable<boolean>[] {
    return dataTypes.map((dataType) => {
      const isPoly = dataType.api.includes('poly');
      return this.http.get(dataType.api).pipe(
        map((response) => this.handleResponse(response, isPoly, dataType.properties, dataType)),
        catchError((error) => {
          console.error(`Error fetching data from ${dataType.api}:`, error);
          return of(false);
        })
      );
    });
  }


  /**
   * Handle the API response and add features to the source.
   * Supports GeoJSON and polyline formats.
   */
  handleResponse(response: any, isPoly: boolean, properties: { [key: string]: string }, dataType: PingDataType): boolean {
    if(response == null) return false;
    try {
      const features: Feature<Geometry>[] = [];
      if (isPoly) {
        // Handle polyline format: geometry in a "shape" property
        response.features.forEach((feature: any) => {
          const geometry = this.polyline.readGeometry(feature.properties.shape, {
            dataProjection: this.dataProjection,
            featureProjection: this.featureProjection
          });
          const enrichedFeature = this.createFeature(geometry, feature.properties, properties, dataType.api); // Use dataType.api as identifier
          features.push(enrichedFeature);
        });
      } else {
        // Handle standard GeoJSON format
        const parsedFeatures = this.geoJSON.readFeatures(response, {
          dataProjection: this.dataProjection,
          featureProjection: this.featureProjection
        }) as Feature<Geometry>[];

        parsedFeatures.forEach((feature) => {
          const geometry = feature.getGeometry();
          const originalProperties = feature.getProperties();
          const enrichedFeature = this.createFeature(geometry, originalProperties, properties, dataType.api); // Use dataType.api as identifier
          features.push(enrichedFeature);
        });
      }
      this.source.addFeatures(features);
      return true;
    } catch (error) {
      console.error('Error processing response:', error);
      return false;
    }
  }


  /**
   * Create a feature with transformed properties and geometry.
   */
  private createFeature(
    geometry: Geometry,
    originalProperties: { [key: string]: any },
    propertyMap: { [key: string]: string },
    dataType: string // Add the dataType identifier
  ): Feature<Geometry> {
    const transformedProperties: { [key: string]: any } = {};

    // Map only properties with valid values
    Object.keys(propertyMap).forEach((key) => {
      const value = originalProperties[key];
      if (value !== undefined && value !== null && value !== '') {
        transformedProperties[propertyMap[key]] = value;
      }
    });

    let feature = new Feature({
      meta: transformedProperties,
      geometry,
    });

    feature.set('slug', this.currentTemporaryMapSlug);
    feature.set('dataType', dataType);
    return feature;
  }


  /**
   * Get the OpenLayers VectorSource with all loaded features.
   */
  getSource(): VectorSource {
    return this.source;
  }

  /**
   * Clear the source to reset the map data.
   */
  clearSource(): void {
    this.source.clear();
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  /**
   * @internal
   */
  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }
}
