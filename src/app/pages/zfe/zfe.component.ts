import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ZFESource } from '@features/map/zfe/zfe.source';
import { MatTabsModule } from '@angular/material/tabs';
import { ZfePrivateVehiclesLayer } from '@features/map/zfe/zfe.vp.layer';
import { ZfeHeavyVehiclesLayer } from '@features/map/zfe/zfe.pl.layer';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { MapService } from '@features/map/map.service';
import { Subject, takeUntil } from 'rxjs';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { formatOpeningHours } from '@helpers/opening-hours.utils';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MapFiltersService } from '@metromobilite/m-map/m-layers';
import { Feature } from 'ol';
import { CardGhostComponent } from "@features/components/ghosts/card-ghost/card-ghost.component";

interface Period {
  start: Date;
  end?: Date;
  label: string;
}

interface DisplayField {
  label: string;
  value: string;
}

@Component({
  selector: 'app-zfe',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatTabsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatCardModule,
    CardGhostComponent
],
  templateUrl: './zfe.component.html',
  styleUrls: ['./zfe.component.scss']
})
export class ZfeComponent implements OnInit, OnDestroy {
  private labelMapping: { [key: string]: string } = {
    vp_critair: 'Véhicules particuliers pour lesquels la circulation est interdite',
    deux_rm_critair: 'Deux-roues motorisés pour lesquels la circulation est interdite',
    vul_critair: 'Véhicules utilitaires légers pour lesquels la circulation est interdite',
    pl_critair: 'Poids lourds pour lesquels la circulation est interdite',
    vp_horaires: 'Horaires de restriction pour véhicules particuliers',
    deux_rm_horaires: 'Horaires de restriction pour deux-roues motorisés',
    vul_horaires: 'Horaires de restriction pour véhicules utilitaires légers',
    pl_horaires: 'Horaires de restriction pour poids lourds',
    url_site: 'Site d’informations',
    url_arrete: 'Arrêté officiel'
  };

  unSubscriber = new Subject<void>();
  selectedIndex = 0;
  selectedVehicleType: 'private' | 'heavy' = 'private';
  selectedPeriod: Period | null = null;
  periods: Period[] = [];
  selectedFeature: any = null;
  displayFields: DisplayField[] = [];
  loading = false;

  constructor(
    private zfeSources: ZFESource,
    private privateLayer: ZfePrivateVehiclesLayer,
    private mapService: MapService,
    private heavyLayer: ZfeHeavyVehiclesLayer,
    private route: ActivatedRoute,
    private router: Router,
    private mapFiltersService: MapFiltersService
  ) { }

  ngOnInit() {
    this.loading = true; // Show loading card initially
    setTimeout(() => {
      this.loading = false; // Hide after 400ms
    }, 600);

    this.route.queryParams.pipe(takeUntil(this.unSubscriber)).subscribe(params => {
      const featureId = params['featureId'];
      const vehicleType = params['vehicleType'] || this.getLastSelectedVehicleType();
      const periodDate = params['period'] ? new Date(params['period']) : new Date();

      this.selectedVehicleType = vehicleType;
      this.selectedIndex = this.selectedVehicleType === 'private' ? 0 : 1;
      this.setLastSelectedVehicleType(this.selectedVehicleType);

      this.zfeSources.getData()
        .pipe(takeUntil(this.unSubscriber))
        .subscribe(() => {
          this.extractPeriods();
          this.selectedPeriod = this.getMatchingPeriod(periodDate);
          this.updateLayerVisibility();
          // this.mapService.fitView(this.zfeSources.getSource().getExtent());

          this.toggleLayers();

          if (featureId) {
            const feature = this.zfeSources.getSource().getFeatureById(featureId);
            if (feature) {
              this.onFeatureClick(feature);
            }
          } else {
            this.selectDefaultFeature();
          }
        });
    });

    this.mapService.featureClicked$
      .pipe(takeUntil(this.unSubscriber))
      .subscribe(featureClicked => {
        if (featureClicked && featureClicked.length > 0 && featureClicked[0].feature && featureClicked[0].feature.get('type') === 'zfeaires') {
          this.onFeatureClick(featureClicked[0].feature);
        }
      });
  }

  ngOnDestroy(): void {
    this.mapService.hideZFEAireLayers();
    this.mapFiltersService.setState([]);
    this.unSubscriber.next();
    this.unSubscriber.complete();
  }

  onVehicleTypeChange(index: number) {
    this.selectedIndex = index;
    this.selectedVehicleType = index === 0 ? 'private' : 'heavy';
    this.setLastSelectedVehicleType(this.selectedVehicleType);
    this.extractPeriods();
    this.selectedPeriod = this.getMatchingPeriod(new Date());
    this.updateLayerVisibility();
    this.toggleLayers();

    if (this.selectedFeature) {
      this.extractDisplayFields();
    }

    this.updateUrlParams();
  }

  onPeriodChange(period: Period) {
    this.loading = true; // Show loading card when the period changes
    setTimeout(() => {
      this.loading = false;
      this.selectedPeriod = period;
      this.updateLayerVisibility();

      const matchingFeature = this.zfeSources.getSource().getFeatures().find(feature => {
        const isRelevantType = this.isFeatureRelevantForType(feature.getProperties());
        const isInPeriod = this.isFeatureInPeriod(feature, this.selectedPeriod!.start);
        return isRelevantType && isInPeriod;
      });

      if (matchingFeature) {
        this.onFeatureClick(matchingFeature);
      }
    }, 400);
  }

  _onPeriodChange(period: Period) {
    this.selectedPeriod = period;
    this.updateLayerVisibility();

    const matchingFeature = this.zfeSources.getSource().getFeatures().find(feature => {
      const isRelevantType = this.isFeatureRelevantForType(feature.getProperties());
      const isInPeriod = this.isFeatureInPeriod(feature, this.selectedPeriod!.start);
      return isRelevantType && isInPeriod;
    });

    if (matchingFeature) {
      this.onFeatureClick(matchingFeature);
    }
  }

  onFeatureClick(feature: Feature) {
    if (feature.getProperties().type !== 'zfeaires' && feature.getProperties().type !== 'zfevoies') {
      return;
    }
    this.selectedFeature = feature.getProperties();
    this.extractDisplayFields();
    this.updateUrlParams(feature.getId() as string);
  }

  private toggleLayers() {
    if (this.selectedVehicleType === 'private') {
      this.mapService.showZfePrivateVehiclesLayer();
      this.mapService.hideZfeHeavyVehiclesLayer();
    } else {
      this.mapService.showZfeHeavyVehiclesLayer();
      this.mapService.hideZfePrivateVehiclesLayer();
    }
  }

  private getMatchingPeriod(date: Date): Period | null {
    const matchedPeriod = this.periods.find(period =>
      (!period.end || date <= period.end) && date >= period.start);
    return matchedPeriod || this.periods[0] || null;
  }

  private selectDefaultFeature() {
    const matchingFeature = this.zfeSources.getSource().getFeatures().find(feature => {
      const properties = feature.getProperties();
      const isZfeAire = properties.type === 'zfeaires';
      if (!isZfeAire) return;
      const isRelevantType = this.isFeatureRelevantForType(properties);
      const isInPeriod = this.isFeatureInPeriod(feature, this.selectedPeriod!.start);
      return isRelevantType && isInPeriod;
    });

    if (matchingFeature) {
      this.onFeatureClick(matchingFeature);
    }
  }

  private isFeatureInPeriod(feature: any, date: Date): boolean {
    const startDate = new Date(feature.getProperties().date_debut);
    const endDate = feature.getProperties().date_fin ? new Date(feature.getProperties().date_fin) : null;
    return (!endDate || date <= endDate) && date >= startDate;
  }

  private setLastSelectedVehicleType(vehicleType: 'private' | 'heavy') {
    localStorage.setItem('lastSelectedVehicleType', vehicleType);
  }

  private getLastSelectedVehicleType(): 'private' | 'heavy' {
    return localStorage.getItem('lastSelectedVehicleType') as 'private' | 'heavy' || 'private';
  }

  private updateLayerVisibility() {
    if (this.selectedPeriod) {
      const date = this.selectedPeriod.start;
      if (this.selectedVehicleType === 'private') {
        this.privateLayer.updateVisibility(date);
      } else {
        this.heavyLayer.updateVisibility(date);
      }
    }

    this.zfeSources.getSource().changed();
  }

  private extractPeriods() {
    this.periods = [];
    const currentDate = new Date();
    const features = this.zfeSources.getSource().getFeatures();
    const uniquePeriods = new Set<string>();

    features.forEach(feature => {
      const properties = feature.getProperties();
      const isZfeAire = properties.type === 'zfeaires';

      if (isZfeAire) {
        const isRelevantType = this.isFeatureRelevantForType(properties);
        if (isRelevantType) {
          const startDate = new Date(properties.date_debut);
          const endDate = properties.date_fin ? new Date(properties.date_fin) : undefined;

          if (!endDate || endDate >= currentDate) {
            const label = endDate ? `${this.formatDate(startDate)} - ${this.formatDate(endDate)}` : `${this.formatDate(startDate)}`;
            if (!uniquePeriods.has(label)) {
              uniquePeriods.add(label);
              this.periods.push({ start: startDate, end: endDate, label });
            }
          }
        }
      }
    });
  }

  private extractDisplayFields() {
    const commonFields = [];
    const specificFields = this.selectedVehicleType === 'private'
      ? ['vp_critair', 'deux_rm_critair', 'vp_horaires', 'deux_rm_horaires']
      : ['vul_critair', 'pl_critair', 'vul_horaires', 'pl_horaires'];

    this.displayFields = [];

    commonFields.forEach(key => {
      const value = this.selectedFeature[key];
      if (value) {
        this.displayFields.push({ label: this.getLabel(key), value });
      }
    });

    specificFields.forEach(key => {
      const value = this.selectedFeature[key];
      if (value) {
        this.displayFields.push({
          label: this.getLabel(key),
          value: key.includes('critair') ? this.formatCritairDescription(value) : key.endsWith('_horaires') ? formatOpeningHours(value) : value
        });
      }
    });
  }

  private formatCritairDescription(value: string): string {
    const order = ['NC', 'V5', 'V4', 'V3', 'V2', 'V1', 'EL'];
    const critairDescriptions: { [key: string]: string } = {
      'NC': "Sans Vignette",
      'V5': "Crit'Air 5",
      'V4': "Crit'Air 4",
      'V3': "Crit'Air 3",
      'V2': "Crit'Air 2",
      'V1': "Crit'Air 1",
      'EL': "Véhicules Électriques"
    };

    const index = order.indexOf(value);
    if (index === -1) return value;

    const restrictedCategories = order.slice(0, index + 1).map(category => critairDescriptions[category]);

    return `${restrictedCategories.join(', ')}`;
  }

  private formatDate(date: Date): string {
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = date.getFullYear();
    return `${day}-${month}-${year}`;
  }

  private getLabel(key: string): string {
    return this.labelMapping[key] || this.formatLabel(key);
  }

  private formatLabel(key: string): string {
    return key.replace(/_/g, ' ').replace(/(^\w|\s\w)/g, m => m.toUpperCase());
  }

  private isFeatureRelevantForType(properties: any): boolean {
    if (properties.type === 'zfevoies') return false;
    if (this.selectedVehicleType === 'private') return properties.vp_critair !== null || properties.deux_rm_critair !== null;
    if (this.selectedVehicleType === 'heavy') return properties.vul_critair !== null || properties.pl_critair !== null;
    return false;
  }

  private updateUrlParams(featureId?: string) {
    const queryParams = {
      featureId,
      vehicleType: this.selectedVehicleType,
      period: this.selectedPeriod ? this.selectedPeriod.start.toISOString().split('T')[0] : null
    };

    // Use replaceUrl to prevent adding to the navigation history
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,
      queryParamsHandling: 'merge',
      replaceUrl: true // This will prevent adding new entries to the history
    });
  }

  trackByFieldLabel(index: number, field: DisplayField): string {
    return field.label;
  }
}
