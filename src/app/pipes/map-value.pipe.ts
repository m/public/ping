import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapValue',
  standalone: true
})
export class MapValuePipe implements PipeTransform {
  transform(value: any, key: string): any {
    const fieldValue = value ? value[key] : null;

    // Only return the key for boolean values that are true
    if (typeof fieldValue === 'boolean') {
      return fieldValue ? key : '';
    }

    // Return the actual value for non-boolean values that are valid (not empty or undefined)
    return (fieldValue !== null && fieldValue !== undefined && fieldValue !== '') ? fieldValue : '';
  }
}