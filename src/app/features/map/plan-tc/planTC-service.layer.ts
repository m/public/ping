import { Injectable } from '@angular/core';
import { LayerInputConfig, MapLayer, MapLayerBase, MapComponent } from '@metromobilite/m-map';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { PlanTCServiceSource } from './planTC-service.source';
import { LineStyleConfig, MlayersLineStyle } from '@metromobilite/m-map/m-layers';

@Injectable({ providedIn: 'root' })
@MapLayer('m-ping:plan-tc')
export class PlanTCServiceLayer extends MapLayerBase {


  constructor(
    public source: PlanTCServiceSource,
    private lineStyle: MlayersLineStyle
  ) { super(); }

  buildLayer(component: MapComponent, config: string | LayerInputConfig): BaseLayer {
    const layer = new VectorLayer({
      source: this.source.getSource() as VectorSource,
      style: (feature, resolution) => {       

        if(!feature.getProperties().visible && !feature.getProperties().selected) return null;
        if(feature.getProperties().resolution && feature.getProperties().resolution !== -1 && feature.getProperties().resolution < resolution && !feature.getProperties().selected) return;

        const useDark = component.tiles.filter(config => config.visible).reduce((_, config) => config.layer.includes('dark'), true);

        const isSelected = feature.get('selected') || false;
        const isHovered = feature.get('hovered') || false;
        
        const someFeatureIsSelected = this.source.getSource().getFeatures().some(f => f.get('selected'));

        const lineStyleConfig: LineStyleConfig = {
          color: `#${feature.getProperties().color}`,
          backgroundColor: '#000',
          noBorder: false,
          width: 4,
          zOffset: feature.getProperties().resolution || 1
        };

        if ((someFeatureIsSelected && !isSelected) ) {          
          lineStyleConfig.color = 'rgba(150, 150, 150, 0.7)';
          if(!useDark) lineStyleConfig.color = 'rgba(240, 240, 240, 0.7)';
          lineStyleConfig.width = 3;
          lineStyleConfig.zOffset = 1;
        }

        if (isSelected || isHovered) {
          lineStyleConfig.color = `#${feature.getProperties().color}`;
          lineStyleConfig.width = 4;
          lineStyleConfig.zOffset = lineStyleConfig.zOffset*2;
        }
        
        return this.lineStyle.build(lineStyleConfig);
      }
    });
    return layer;
  }
}
