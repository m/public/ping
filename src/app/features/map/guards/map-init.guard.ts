import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { MapService } from '../map.service';

@Injectable({
	providedIn: 'root'
})
export class MapInitGuard implements CanActivate {
	path: ActivatedRouteSnapshot[]; route: ActivatedRouteSnapshot;

	constructor(private mapService: MapService) { }

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		if (this.mapService.initialized) {
			return true;
		}
		return new Promise((resolve) => {
			this.mapService.initMap.subscribe((isInit: boolean) => {
				if (isInit) resolve(true);
			});
		});
	}
}
