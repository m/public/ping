import { Injectable } from '@angular/core';
import { LayerInputConfig, MapLayer, MapLayerBase, MapComponent } from '@metromobilite/m-map';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { PlanTCServiceSource } from './planTC-service.source';
import { LineStyleConfig, MlayersCircleStyle, MlayersLineStyle, MlayersTextStyle, TextStyleConfig } from '@metromobilite/m-map/m-layers';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService, FRONT_CONFIG } from '@metromobilite/m-features/core';
import { ConfigPingService } from '@services/config.service';

@Injectable({ providedIn: 'root' })
@MapLayer('m-ping:clusters')
export class ClusterServiceLayer extends MapLayerBase {

  layersConfig : any = {};

  constructor(
    public source: PlanTCServiceSource,
    private lineStyle: MlayersLineStyle,
    private textStyle: MlayersTextStyle,
    private circleStyle: MlayersCircleStyle,
    private configPingService: ConfigPingService,
    private router: Router
  ) { super(); }

  buildLayer(component: MapComponent, config: string | LayerInputConfig): BaseLayer {
    const layer = new VectorLayer({
      source: this.source.getSourceClusters() as VectorSource,
      style: (feature, resolution) => {               
        
        this.layersConfig = this.configPingService.finalConfig.layers.cluster;        
        
        let styleExist : boolean = false;

        const isVisibleMap = feature.get('visibleMap') || false;
        const isHovered = feature.get('hovered') || false;
        const isSelected = feature.get('selected') || false;
        const someFeatureIsVisibleMap = this.source.getSourceClusters().getFeatures().some(f => f.get('visibleMap'));
        const useDark = component.tiles.filter(config => config.visible).reduce((_, config) => config.layer.includes('dark'), true);

        let fillColor = "#FFFFFF";
				let strokeColor = "#000000";
				if(!useDark) {
					fillColor = "#000000";
					strokeColor = "#FFFFFF";	
				}
        let styles = [];

        let circleConfig : any = {
          zIndex: isSelected || isVisibleMap ? 150 : 100,
          color: fillColor,
          strokeWidth: 3,
          strokeColor: strokeColor,
          radius: 5,
        }

        // //Mettre le style par defaut du poi en fonction de la config      
        for (const resolutionConfig in this.layersConfig.default.resolution) {
          if (resolution <= Number(resolutionConfig)) {
            styleExist = false;
            if(typeof this.layersConfig.default.resolution[resolutionConfig].poi.visible == 'undefined' || this.layersConfig.default.resolution[resolutionConfig].poi.visible) {
              styleExist = true;
              circleConfig = {...circleConfig,...this.layersConfig.default.resolution[resolutionConfig].poi.global};
              if(typeof this.layersConfig.default.resolution[resolutionConfig].poi.dark !== 'undefined' && useDark) circleConfig = {...circleConfig,...this.layersConfig.default.resolution[resolutionConfig].poi.dark};
              if(typeof this.layersConfig.default.resolution[resolutionConfig].poi.light !== 'undefined' && !useDark) circleConfig = {...circleConfig,...this.layersConfig.default.resolution[resolutionConfig].poi.light};
              break;
            }
          }
        }


        // //Mettre le style si des éléments sont hover ou visible
        for (const resolutionConfig in this.layersConfig.someVisible.resolution) {
          if(someFeatureIsVisibleMap && this.layersConfig.someVisible) {
            if (resolution <= Number(resolutionConfig)) {
              styleExist = false;
              if(typeof this.layersConfig.someVisible.resolution[resolutionConfig].poi.visible == 'undefined' || this.layersConfig.someVisible.resolution[resolutionConfig].poi.visible) {
                styleExist = true;
                circleConfig = {...circleConfig,...this.layersConfig.someVisible.resolution[resolutionConfig].poi.global};
                if(typeof this.layersConfig.someVisible.resolution[resolutionConfig].poi.dark !== 'undefined' && useDark) circleConfig = {...circleConfig,...this.layersConfig.someVisible.resolution[resolutionConfig].poi.dark};
                if(typeof this.layersConfig.someVisible.resolution[resolutionConfig].poi.light !== 'undefined' && !useDark) circleConfig = {...circleConfig,...this.layersConfig.someVisible.resolution[resolutionConfig].poi.light};                
                break;
              }
            }
          }
        }

        // //Match entre la config des poi et la donnée des features
        for (const event in this.layersConfig){
          for (const resolutionConfig in this.layersConfig[event].resolution) {
            if(feature.getProperties()[event]) {
              if (resolution <= Number(resolutionConfig)) {
                styleExist = false;
                if(typeof this.layersConfig[event].resolution[resolutionConfig].poi.visible == 'undefined' || this.layersConfig[event].resolution[resolutionConfig].poi.visible) {
                  styleExist = true;
                  circleConfig = {...circleConfig,...this.layersConfig[event].resolution[resolutionConfig].poi.global};
                  if(typeof this.layersConfig[event].resolution[resolutionConfig].poi.dark !== 'undefined' && useDark) circleConfig = {...circleConfig,...this.layersConfig[event].resolution[resolutionConfig].poi.dark};
                  if(typeof this.layersConfig[event].resolution[resolutionConfig].poi.light !== 'undefined' && !useDark) circleConfig = {...circleConfig,...this.layersConfig[event].resolution[resolutionConfig].poi.light};                
                  break;
                }
              }
            }
          }
        }

        if(!styleExist) return null;
        
        const circleStyle = this.circleStyle.build(circleConfig as any);        
        styles.push(circleStyle);




        ///////////////////////////////////
        //Gestion de l'affichage du texte       

        let textConfig = {
          zIndex: isSelected || isVisibleMap ? 150 : 100,
          font: '13px Roboto,Verdana,Calibri,sans-serif',
          text: feature.get('name'),
          textAlign: 'center',
          textBaseline: 'middle',
          offsetX: 0,
          offsetY: 20,
          fillColor: fillColor,
          strokeColor: strokeColor,
          strokeWidth: 5
        }

        // //Mettre le style par defaut du texts en fonction de la config
        for (const resolutionConfig in this.layersConfig.default.resolution) {
          if (resolution <= Number(resolutionConfig)) {
            styleExist = false;
            if(typeof this.layersConfig.default.resolution[resolutionConfig].text.visible == 'undefined' || this.layersConfig.default.resolution[resolutionConfig].text.visible) {
              styleExist = true;
              textConfig = {...textConfig,...this.layersConfig.default.resolution[resolutionConfig].text.global};
              if(typeof this.layersConfig.default.resolution[resolutionConfig].text.dark !== 'undefined' && useDark) textConfig = {...textConfig,...this.layersConfig.default.resolution[resolutionConfig].text.dark};
              if(typeof this.layersConfig.default.resolution[resolutionConfig].text.light !== 'undefined' && !useDark) textConfig = {...textConfig,...this.layersConfig.default.resolution[resolutionConfig].text.light};
              
              const labelConfig = this.layersConfig.default.resolution[resolutionConfig].text.global.label;
              if(labelConfig.static && labelConfig.valeur) textConfig.text = labelConfig.valeur;
              if(labelConfig.static && labelConfig.valeur) textConfig.text = feature.get(labelConfig.valeur) || '';
              break;
            }
          }
        }


        // //Mettre le style si des éléments sont hover ou visible
        if(someFeatureIsVisibleMap && this.layersConfig.someVisible && !isVisibleMap && !isHovered){
          for (const resolutionConfig in this.layersConfig.someVisible.resolution) {
            if (resolution <= Number(resolutionConfig)) {
              styleExist = false;
              if(typeof this.layersConfig.someVisible.resolution[resolutionConfig].text.visible == 'undefined' || this.layersConfig.someVisible.resolution[resolutionConfig].text.visible) {
                styleExist = true;
                textConfig = {...textConfig,...this.layersConfig.someVisible.resolution[resolutionConfig].text.global};
                if(typeof this.layersConfig.someVisible.resolution[resolutionConfig].text.dark !== 'undefined' && useDark) textConfig = {...textConfig,...this.layersConfig.someVisible.resolution[resolutionConfig].text.dark};
                if(typeof this.layersConfig.someVisible.resolution[resolutionConfig].text.light !== 'undefined' && !useDark) textConfig = {...textConfig,...this.layersConfig.someVisible.resolution[resolutionConfig].text.light};
                
                const labelConfig = this.layersConfig.someVisible.resolution[resolutionConfig].text.global.label;
                if(labelConfig.static && labelConfig.valeur) textConfig.text = labelConfig.valeur;
                if(labelConfig.static && labelConfig.valeur) textConfig.text = feature.get(labelConfig.valeur) || '';
                break;
              }
            }
          }
        }

        // //Match entre la config des textes et la donnée des features
        for (const event in this.layersConfig) {
          if(feature.getProperties()[event]) {
            for (const resolutionConfig in this.layersConfig[event].resolution) {
              if (resolution <= Number(resolutionConfig)) {
                styleExist = false;
                if(typeof this.layersConfig[event].resolution[resolutionConfig].text.visible == 'undefined' || this.layersConfig[event].resolution[resolutionConfig].text.visible) {
                  styleExist = true;
                  textConfig = {...textConfig,...this.layersConfig[event].resolution[resolutionConfig].text.global};
                  if(typeof this.layersConfig[event].resolution[resolutionConfig].text.dark !== 'undefined' && useDark) textConfig = {...textConfig,...this.layersConfig[event].resolution[resolutionConfig].text.dark};
                  if(typeof this.layersConfig[event].resolution[resolutionConfig].text.light !== 'undefined' && !useDark) textConfig = {...textConfig,...this.layersConfig[event].resolution[resolutionConfig].text.light};
                  
                  const labelConfig = this.layersConfig[event].resolution[resolutionConfig].text.global.label;
                  if(labelConfig.static && labelConfig.valeur) textConfig.text = labelConfig.valeur;
                  if(labelConfig.static && labelConfig.valeur) textConfig.text = feature.get(labelConfig.valeur) || '';
                  break;
                }
              }
            }
          }
        }

        if(!styleExist) return styles;
        let textAffiche = this.textStyle.build(textConfig as TextStyleConfig);
        styles.push(textAffiche);

        return styles
      }
    });
    return layer;
  }
}
