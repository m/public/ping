import { Component, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WinterServiceReportSource } from '@features/map/winter-service/winter-service.source';
import { WinterServiceReport, WinterServiceReportMeasure } from '@metromobilite/m-features/dyn';
import { MapService } from '@features/map/map.service';
import VectorSource from 'ol/source/Vector';
import { Subject, takeUntil } from 'rxjs';
import { Feature } from 'ol';
import { CirculationConditionsMap, LimitePluieNeige, WINTER_SERVICE_CATEGORIES } from 'src/app/models/winter-service.model';
import { MapValuePipe } from '@pipes/map-value.pipe';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { CommonModule } from '@angular/common';
import { MapClickedFeature } from '@features/map/map.model';
import { CardGhostComponent } from "@features/components/ghosts/card-ghost/card-ghost.component";
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { SnackbarComponent } from '@features/components/snackbar/snackbar.component';
import { MatTooltipModule } from '@angular/material/tooltip';

interface CategoryItem {
  key: string;
  label: string;
  value: any;
}

@Component({
  selector: 'app-vh',
  standalone: true,
  imports: [CommonModule, MatDividerModule, MatIconModule, MatCardModule, CardGhostComponent, MatSnackBarModule, MatTooltipModule],
  templateUrl: './vh.component.html',
  styleUrls: ['./vh.component.scss']
})
export class VhComponent implements OnInit, OnDestroy {
  dynWinterServiceReport: WinterServiceReport;
  unSubscriber = new Subject<void>();
  winterServiceReportMeasure: WinterServiceReportMeasure;
  private highlightedFeature: Feature | null = null;
  private selectedFeature: Feature | null = null;
  loading: boolean = false;  // Loading state for ghost elements

  winterServiceReportMeasureCategories = WINTER_SERVICE_CATEGORIES;
  circulationConditionsMap = CirculationConditionsMap;
  displayableCategories: { [key: string]: CategoryItem[] } = {};

  public limitePluieNeige = [];
  limitePluieNeigeKeys = Object.keys(LimitePluieNeige);
  userReadWarning: boolean = false;
  private snackbarOpen: boolean = false;

  constructor(
    private winterServiceSource: WinterServiceReportSource,
    private mapService: MapService,
    private router: Router,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    let featureIdFromUrl: string | null = null;

    this.route.queryParams.subscribe(params => {
      featureIdFromUrl = params['selectedReportMeasure'] || null;
    });

    this.winterServiceSource.getData()
      .pipe(takeUntil(this.unSubscriber))
      .subscribe(() => {
        // this.mapService.fitView((this.winterServiceSource.getSource() as VectorSource).getExtent());
        this.mapService.showWinterServiceLayer();

        if (featureIdFromUrl) {
          this.selectFeatureById(featureIdFromUrl);
        }
      });

    /* this.winterServiceSource.update$
      .pipe(takeUntil(this.unSubscriber))
      .subscribe((updatedResponse) => {
        if (updatedResponse) {
          this.limitePluieNeige = [];
          this.dynWinterServiceReport = updatedResponse;
          const isReportDataInWarningPeriod = this.isTimeOlderThan24Hours(this.dynWinterServiceReport.time)
          if (this.dynWinterServiceReport && !this.userReadWarning && isReportDataInWarningPeriod) {
            const icon = 'error_outline';
            const message = 'Les données affichées datent de plus de 24 heures. Elles pourraient ne plus être à jour.';
            const actionText = 'Fermer';
            const actionCallback = () => {
              this.userReadWarning = true;
            };
            this.snackBar.openFromComponent(SnackbarComponent, {
              data: {
                icon,
                message,
                actionText,
                actionCallback
              },
              verticalPosition: 'top',
              panelClass: ['dark-overlay-1', 'light-overlay-inner']
            });
          }
          this.limitePluieNeigeKeys.forEach(key => {
            if (updatedResponse.data[key]) {
              this.limitePluieNeige.push({ label: LimitePluieNeige[key], value: updatedResponse.data[key] });
            }
          });
          this.cdr.markForCheck();
        }
      }); */
    this.winterServiceSource.update$
      .pipe(takeUntil(this.unSubscriber))
      .subscribe((updatedResponse) => {
        if (updatedResponse) {
          this.handleDataUpdate(updatedResponse);
        }
      });

    this.mapService.featureClicked$
      .pipe(takeUntil(this.unSubscriber))
      .subscribe((featureClicked: MapClickedFeature[] | null) => {
        if (featureClicked && featureClicked.length > 0 && featureClicked[0].feature) {
          this.selectFeature(featureClicked[0].feature);
        } else {
          this.resetFeatureStyles();
        }
      });

    this.mapService.featureHovered$
      .pipe(takeUntil(this.unSubscriber))
      .subscribe((featureClicked: MapClickedFeature | null) => {
        if (this.highlightedFeature && (!featureClicked || !featureClicked.feature || featureClicked.feature !== this.highlightedFeature)) {
          this.highlightedFeature.set('hovered', false);
          this.highlightedFeature = null;
        }

        if (featureClicked && featureClicked.feature && featureClicked.feature !== this.highlightedFeature) {
          featureClicked.feature.set('hovered', true);
          this.highlightedFeature = featureClicked.feature;
        }
      });
  }

  ngOnDestroy(): void {
    this.resetFeatureStyles(false);
    this.snackBar.dismiss();
    this.mapService.hideWinterServiceLayer();
    this.unSubscriber.next();
    this.unSubscriber.complete();
  }

  selectFeature(feature: Feature): void {
    if (this.selectedFeature) {
      this.selectedFeature.set('selected', false);
    }

    this.loading = true;
    this.cdr.markForCheck();

    setTimeout(() => {
      this.selectedFeature = feature;
      this.selectedFeature.set('selected', true);
      this.winterServiceReportMeasure = feature.getProperties() as WinterServiceReportMeasure;

      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: { selectedReportMeasure: feature.getId() },
        queryParamsHandling: 'merge',
        replaceUrl: true
      });

      this.updateDisplayableCategories();
      this.loading = false;
      this.cdr.markForCheck();
    }, 350);
  }

  selectFeatureById(featureId: string): void {
    const feature = this.winterServiceSource.getSource().getFeatureById(featureId);
    if (feature) {
      this.selectFeature(feature);
    }
  }

  updateDisplayableCategories(): void {
    this.displayableCategories = Object.keys(this.winterServiceReportMeasureCategories)
      .reduce((filteredCategories, categoryKey) => {
        const categoryItems = this.winterServiceReportMeasureCategories[categoryKey];
        const displayableItems = categoryItems.map(item => {
          let value = this.winterServiceReportMeasure[item.key];

          if (item.key === 'conditions_circulation' && typeof value === 'string') {
            value = this.circulationConditionsMap[value.toLowerCase()] || value;
          }

          return { ...item, value };
        }).filter(item => {
          const value = item.value;
          return value === true || (value !== false && value !== null && value !== undefined && value !== '');
        });

        if (displayableItems.length > 0) {
          filteredCategories[categoryKey] = displayableItems;
        }

        return filteredCategories;
      }, {} as { [key: string]: CategoryItem[] });
  }

  resetFeatureStyles(updateUrl = true): void {
    if (this.selectedFeature) {
      this.selectedFeature.set('selected', false);
      this.selectedFeature = null;
    }

    if (this.highlightedFeature) {
      this.highlightedFeature.set('hovered', false);
      this.highlightedFeature = null;
    }

    this.winterServiceReportMeasure = null as any;
    this.displayableCategories = {};

    if (updateUrl) {
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: { selectedReportMeasure: null },
        queryParamsHandling: 'merge',
        replaceUrl: true
      });
    }

    this.cdr.markForCheck();
  }

  private isTimeOlderThan24Hours(reportTime: Date | string | null): boolean {
    if (!reportTime) {
      return false;
    }

    const time = new Date(reportTime).getTime();
    const currentTime = new Date().getTime();
    const timeDifference = currentTime - time;
    const warningDurationThreshold = 24 * 60 * 60 * 1000; // 24 hours in milliseconds

    return timeDifference > warningDurationThreshold;
  }

  private handleDataUpdate(updatedResponse: WinterServiceReport): void {
    this.limitePluieNeige = [];
    this.dynWinterServiceReport = updatedResponse;

    const isReportDataInWarningPeriod = this.isTimeOlderThan24Hours(this.dynWinterServiceReport.time);

    this.userReadWarning = localStorage.getItem('userReadWarning') === 'true';


    // Show the snackbar only if it hasn't been shown before and the user hasn't read the warning
    if (isReportDataInWarningPeriod && !this.userReadWarning && !this.snackbarOpen) {
      this.showSnackbar();
    }

    // Update limitePluieNeige
    this.limitePluieNeigeKeys.forEach(key => {
      if (updatedResponse.data[key]) {
        this.limitePluieNeige.push({ label: LimitePluieNeige[key], value: updatedResponse.data[key] });
      }
    });
    this.cdr.markForCheck();
  }

  private showSnackbar(): void {
    const icon = 'error_outline';
    const message = 'Les données affichées datent de plus de 24 heures. Elles pourraient ne plus être à jour.';
    const actionText = 'Fermer';
    const actionCallback = () => {
      this.userReadWarning = true;
      localStorage.setItem('userReadWarning', 'true'); // Save to localStorage
    };

    this.snackbarOpen = true; // Mark the snackbar as open
    const snackbarRef = this.snackBar.openFromComponent(SnackbarComponent, {
      data: { icon, message, actionText, actionCallback },
      verticalPosition: 'top',
      panelClass: []
    });

    // When the snackbar is dismissed, reset the snackbarOpen flag
    snackbarRef.afterDismissed().subscribe(() => {
      this.snackbarOpen = false;
    });
  }
}
