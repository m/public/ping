import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanTCService } from '@pages/plan-tc/plan-tc.service';
import { CoreModule, Line } from '@metromobilite/m-features/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MLogoLines } from '@metromobilite/m-ui';

@Component({
  selector: 'app-logo-ligne',
  standalone: true,
  imports: [CommonModule, CoreModule, MatCheckboxModule, MLogoLines],
  templateUrl: './logo-ligne.component.html',
  styleUrls: ['./logo-ligne.component.scss']
})
export class LogoLigneComponent {

  @Input() line : Line;
  @Input() descriptionAffichee : boolean = false;

  constructor(public planTcService : PlanTCService) {

  }

  selected(line : Line) {
    return this.planTcService.isSelected(line);
  }
}
