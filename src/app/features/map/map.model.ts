import { PingTile } from '@metromobilite/m-features/core';
import { Feature, MapBrowserEvent } from 'ol';
import { Coordinate } from 'ol/coordinate';
import { Layer } from 'ol/layer';

export class MapEvent {
	constructor(public originalEvent: MapBrowserEvent<UIEvent>) {
	}
}

export class MapFeatureClickedEvent extends MapEvent {
	constructor(public feature: Feature, public layer: Layer, public originalEvent: MapBrowserEvent<UIEvent>) {
		super(originalEvent);
	}
}

export class MapMoveStartEvent extends MapEvent {
	constructor(public originalEvent: MapBrowserEvent<UIEvent>) {
		super(originalEvent);
	}
}

export class MapMoveEndEvent extends MapEvent {
	constructor(public originalEvent: MapBrowserEvent<UIEvent>) {
		super(originalEvent);
	}
}

export class MapPostRenderEvent extends MapEvent {
	constructor(public originalEvent: MapBrowserEvent<UIEvent>) {
		super(originalEvent);
	}
}

export class MapRenderCompleteEvent extends MapEvent {
	constructor(public originalEvent: MapBrowserEvent<UIEvent>) {
		super(originalEvent);
	}
}

export interface MapPositionOptions {
	zoom?: number;
	center?: number[];
	duration?: number;
}

export interface MapClickedFeature {
	feature: Feature;
	coordinate: Coordinate;
}

export interface PopUpType {
	data: any;
	typePopUp: string;
}


// TODO: M Features? 

export interface PingConfig {
	tiles: PingTile[];
	layers: PingLayersConfig;
	temporaryMaps: Record<string, PingTemporaryMap>;
  }
  
  export interface PingLayersConfig {
	cluster: PingClusterConfig;
	someVisible: PingResolutionConfig;
	visibleMap: PingResolutionConfig;
	selected: PingResolutionConfig;
  }
  
  export interface PingClusterConfig {
	default: PingResolutionConfig;
  }
  
  export interface PingResolutionConfig {
	resolution: Record<string, PingPoiConfig | PingTextConfig>;
  }
  
  export interface PingPoiConfig {
	global?: PingGlobalStyle;
	dark?: PingThemeStyle;
	light?: PingThemeStyle;
  }
  
  export interface PingTextConfig {
	global?: PingTextStyle;
	dark?: PingThemeStyle;
	light?: PingThemeStyle;
	visible?: boolean;
  }
  
  export interface PingGlobalStyle {
	strokeWidth?: number;
	radius?: number;
	zIndex?: number;
	color?: string;
	strokeColor?: string;
  }
  
  export interface PingThemeStyle {
	color?: string;
	strokeColor?: string;
	fillColor?: string;
	strokeWidth?: number;
	radius?: number; 
	lineDash?: number[]; 
	lineCap?: string; 
	borderWidth?: number;
	borderStrokeColor?: string;
	zIndex?: number;
	alternativeColor?: string;
  }
  
  export interface PingTextStyle {
	label: {
	  valeur: string;
	  static: boolean;
	};
	zIndex?: number;
	font?: string;
	textAlign?: string;
	textBaseline?: string;
	offsetX?: number;
	offsetY?: number;
	strokeWidth?: number;
	fillColor?: string;
	strokeColor?: string;
  }
  
  export interface PingTemporaryMap {
	dataTypes: PingDataType[];
	description?: string;
  }
  
  export interface PingDataType {
	api: string;
	properties: Record<string, string>;
	style: PingStyleConfig;
  }
  
  export interface PingStyleConfig {
	dark: PingThemeStyle;
	light: PingThemeStyle;
  }
