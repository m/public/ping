import { AsyncPipe, NgClass, NgIf } from '@angular/common';
import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { THEMES } from '@metromobilite/m-features/core';
import { LayerInputConfig, MapComponent, MapLayersService, MMapModule } from '@metromobilite/m-map';
import { MapFiltersService } from '@metromobilite/m-map/m-layers';
import { Feature, Map, MapBrowserEvent, Overlay, View } from 'ol';
import BaseEvent from 'ol/events/Event';
import { Subject, takeUntil } from 'rxjs';
import { MapService } from '../map.service';
import { Coordinate } from 'ol/coordinate';
import { PopupMapComponent } from '@features/components/popup-map/popup-map.component';
import { MapClickedFeature } from '../map.model';
import { MatIconModule } from '@angular/material/icon';
import { MatAccordion, MatExpansionModule } from '@angular/material/expansion';
import { ThemeToggleService } from '@services/theme-toggle.service';
import { PlanTCServiceSource } from '../plan-tc/planTC-service.source';
import { WinterServiceReportSource } from '../winter-service/winter-service.source';
import { ZFESource } from '../zfe/zfe.source';
import { PopupMapDirective } from '@features/components/popup-map/popup-map.directive';
import { TemporaryMapSource } from '../temporary-maps/temporary-maps.source';


@Component({
	selector: 'app-map-wrapper',
	templateUrl: './map-wrapper.component.html',
	styleUrls: ['./map-wrapper.component.scss'],
	standalone: true,
	imports: [PopupMapDirective, MMapModule, NgIf, MatButtonModule, NgClass, PopupMapComponent, MatButtonModule, MatIconModule, MatExpansionModule, AsyncPipe],
})
export class MapWrapperComponent implements AfterViewInit, OnDestroy {

	tiles: LayerInputConfig[] = [
		{ layer: 'core:xyz-light', visible: false },
		{ layer: 'core:xyz-dark', visible: true },
	];
	layers: LayerInputConfig[] = [];
	zoom = 12;
	displayCenter = false;
	private winterServiceLayer: LayerInputConfig;
	private zfePrivateVehiclesLayer: LayerInputConfig;
	private zfeHeavyVehiclesLayer: LayerInputConfig;
	private planTCServiceLayer: LayerInputConfig;
	private clusterServiceLayer: LayerInputConfig;
	private temporaryMapLayer: LayerInputConfig;
	private unSubscriber = new Subject<void>();
	private popupOverlay: Overlay;


	set theme(value: THEMES) {
		this.tiles.forEach((tile) => {
			switch (tile.layer) {
				case 'core:xyz-light':
					tile.visible = value === THEMES.LIGHT;
					break;
				case 'core:xyz-dark':
				default:
					tile.visible = value === THEMES.DARK;
					break;
			}
		});
		this.tiles = JSON.parse(JSON.stringify(this.tiles));
		
		this.planTcSource.changed();
		this.winterSource.changed();
		this.zfeSource.changed();
		this.temporaryMapSource.changed();
	}

	@ViewChild('map', { static: true }) mapComponent!: MapComponent;
	@ViewChild('popup', {static: true}) popupRef: ElementRef;
	@ViewChild('description', {static: true}) descriptionRef: ElementRef;
	constructor(private mapFiltersService: MapFiltersService, private layersService: MapLayersService,
		protected mapService: MapService, private themeService : ThemeToggleService, private temporaryMapSource: TemporaryMapSource,
		private planTcSource: PlanTCServiceSource, private winterSource: WinterServiceReportSource, private zfeSource : ZFESource) 
	{
			this.theme = this.themeService.theme;
	}

	ngAfterViewInit(): void {
		this.mapService.setMap(this);
		this.mapFiltersService
			.init(this.mapComponent)
			.pipe(takeUntil(this.unSubscriber))
			.subscribe(() => {

				if (this.mapFiltersService.initialized) {
					this.winterServiceLayer = { layer: 'm-ping:winter-service', visible: false };
					this.zfePrivateVehiclesLayer = { layer: 'm-ping:zfe-private-vehicles', visible: false };
					this.zfeHeavyVehiclesLayer = { layer: 'm-ping:zfe-heavy-vehicles', visible: false };
					this.planTCServiceLayer = { layer: 'm-ping:plan-tc', visible: false };
					this.clusterServiceLayer = { layer: 'm-ping:clusters', visible: false };
					this.temporaryMapLayer = { layer: 'm-ping:temporary-map', visible: false };
					this.layers = [
						this.winterServiceLayer, this.zfePrivateVehiclesLayer, 
						this.zfeHeavyVehiclesLayer, this.planTCServiceLayer, 
						this.clusterServiceLayer, this.temporaryMapLayer
					];
				}
				// Waiting for layers init.
				setTimeout(() => {
					this.mapService.initMap.next(this.mapFiltersService.initialized);
				});
			});
		this.initPopupOverlay();
	}

	ngOnDestroy(): void {
		this.unSubscriber.next();
		this.unSubscriber.complete();
	}

	get map(): Map {
		return this.mapComponent.map;
	}

	get view(): View {
		return this.mapComponent.view;
	}

	get center(): Coordinate {
		return this.view.getCenter() || [];
	}

	initPingMap() {
		this.mapService.setMap(this);
		this.mapFiltersService
			.init(this.mapComponent)
			.pipe(takeUntil(this.unSubscriber))
			.subscribe(() => {
				if (this.mapFiltersService.initialized) {
				}
				// Waiting for layers init.
				setTimeout(() => {
				});
			});
	}

	onMapClick(event: BaseEvent): void {
		let featuresCLicked: MapClickedFeature[] = [];
		if (event instanceof MapBrowserEvent) {		
			event.map.forEachFeatureAtPixel(event.pixel, (feature: Feature, layer) => {
				featuresCLicked.push({feature, coordinate: event.coordinate});
			});
			
			let isClickout = featuresCLicked.length === 0;
			
			if (isClickout) {
				this.mapService.emitFeatureClicked(null);
			}
			else {
				this.mapService.emitFeatureClicked(featuresCLicked);
			}
		}
	}

	onPointerMove(event: BaseEvent): void {
		if (event instanceof MapBrowserEvent) {
			if (event.dragging) {
				return; // Do nothing when dragging
			}

			let foundFeature = false;
			event.map.forEachFeatureAtPixel(event.pixel, (feature: Feature) => {
				this.mapService.emitFeatureHovered({feature, coordinate: event.coordinate});
				foundFeature = true;
				if (foundFeature) event.map.getTargetElement().style.cursor = 'pointer';
			});

			if (!foundFeature) {
				// No feature found at the pixel, emit null
				this.mapService.emitFeatureHovered(null);
				event.map.getTargetElement().style.cursor = '';
			}
		}
	}

	private initPopupOverlay(): void {
		this.popupOverlay = new Overlay({
			element: this.popupRef.nativeElement,
			stopEvent: true,
			offset: [10, 25]
		});
		this.mapComponent.map.addOverlay(this.popupOverlay);
	}


	// zoomIn(){
	// 	this.mapComponent.view.animate({
	// 		zoom: this.mapComponent.view.getZoom() + 1,
	// 		duration: 200
	// 	});
	// }

	// zoomOut(){
	// 	this.mapComponent.view.animate({
	// 		zoom: this.mapComponent.view.getZoom() - 1,
	// 		duration: 200
	// 	});
	// }

	reloadAllSources(){
		this.planTcSource.changed();
		this.winterSource.changed();
		this.zfeSource.changed();
		this.temporaryMapSource.changed();
	}

	hidePopup(): void {
		this.popupOverlay.setPosition(undefined);
		this.popupOverlay.getElement().style.display = 'none';
	}

	showPopUp(coordinate: Coordinate): void {		
		this.popupOverlay.setPosition(coordinate);
		this.popupOverlay.getElement().style.display = 'flex';
	}

	showZfePrivateVehiclesLayer() {
		if (this.zfePrivateVehiclesLayer && this.zfePrivateVehiclesLayer._uid) {
			this.layersService.show(this.mapComponent, this.zfePrivateVehiclesLayer._uid);
		}
	}

	showZfeHeavyVehiclesLayer() {
		if (this.zfeHeavyVehiclesLayer && this.zfeHeavyVehiclesLayer._uid) {
			this.layersService.show(this.mapComponent, this.zfeHeavyVehiclesLayer._uid);
		}
	}

	hideZfePrivateVehiclesLayer() {
		if (this.zfePrivateVehiclesLayer && this.zfePrivateVehiclesLayer._uid) {
			this.layersService.hide(this.mapComponent, this.zfePrivateVehiclesLayer._uid);
		}

	}

	hideZfeHeavyVehiclesLayer() {
		if (this.zfeHeavyVehiclesLayer && this.zfeHeavyVehiclesLayer._uid) {
			this.layersService.hide(this.mapComponent, this.zfeHeavyVehiclesLayer._uid);
		}
	}


	showZFEAireLayers() {
		if (this.zfeHeavyVehiclesLayer && this.zfeHeavyVehiclesLayer._uid) {
			this.layersService.show(this.mapComponent, this.zfeHeavyVehiclesLayer._uid);
		}
		if (this.zfePrivateVehiclesLayer && this.zfePrivateVehiclesLayer._uid) {
			this.layersService.show(this.mapComponent, this.zfePrivateVehiclesLayer._uid);
		}
	}

	hideZFEAireLayers() {
		if (this.zfeHeavyVehiclesLayer && this.zfeHeavyVehiclesLayer._uid) {
			this.layersService.hide(this.mapComponent, this.zfeHeavyVehiclesLayer._uid);
		}
		if (this.zfePrivateVehiclesLayer && this.zfePrivateVehiclesLayer._uid) {
			this.layersService.hide(this.mapComponent, this.zfePrivateVehiclesLayer._uid);
		}
	}

	showWinterServiceLayer() {
		if (this.winterServiceLayer && this.winterServiceLayer._uid) {
			this.layersService.show(this.mapComponent, this.winterServiceLayer._uid);
		}
	}

	hideWinterServiceLayer() {
		if (this.winterServiceLayer && this.winterServiceLayer._uid) {
			this.layersService.hide(this.mapComponent, this.winterServiceLayer._uid);
		}
	}

	showPlanTCServiceLayer() {
		if (this.planTCServiceLayer && this.planTCServiceLayer._uid) {
			this.layersService.show(this.mapComponent, this.planTCServiceLayer._uid);
		}
	}

	hidePlanTCServiceLayer() {
		if (this.planTCServiceLayer && this.planTCServiceLayer._uid) {
			this.layersService.hide(this.mapComponent, this.planTCServiceLayer._uid);
		}
	}

	showClustersServiceLayer() {
		if (this.clusterServiceLayer && this.clusterServiceLayer._uid) {
			this.layersService.show(this.mapComponent, this.clusterServiceLayer._uid);
		}
	}

	hideClustersServiceLayer() {
		if (this.clusterServiceLayer && this.clusterServiceLayer._uid) {
			this.layersService.hide(this.mapComponent, this.clusterServiceLayer._uid);
		}
	}

	showTemporaryLayer() {
		if (this.temporaryMapLayer && this.temporaryMapLayer._uid) {
			this.layersService.show(this.mapComponent, this.temporaryMapLayer._uid);
		}
	}

	hideTemporaryLayer() {
		if (this.temporaryMapLayer && this.temporaryMapLayer._uid) {
			this.layersService.hide(this.mapComponent, this.temporaryMapLayer._uid);
		}
	}

}