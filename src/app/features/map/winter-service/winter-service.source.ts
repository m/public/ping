import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Section, WinterServiceReport, WinterServiceReportMeasure, WinterServiceReportService } from '@metromobilite/m-features/dyn';
import { MAP_PROJECTION_DESTINATION, MAP_PROJECTION_SOURCE, MapSourceBase } from '@metromobilite/m-map';
import { GeometryService } from '@metromobilite/m-map/m-layers';
import { Feature } from 'ol';
import Geometry from 'ol/geom/Geometry';
import Source from 'ol/source/Source';
import VectorSource from 'ol/source/Vector';
import { Observable, of, zip, interval, BehaviorSubject } from 'rxjs';
import { catchError, map, switchMap, startWith, takeUntil } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class WinterServiceReportSource extends MapSourceBase {

  source: VectorSource;
  private updateInterval$: Observable<any>;
  private updateSubject = new BehaviorSubject<WinterServiceReport | null>(null);

  constructor(
    private http: HttpClient,
    @Inject(MAP_PROJECTION_SOURCE) private dataProjection: string,
    @Inject(MAP_PROJECTION_DESTINATION) private featureProjection: string,
    private winterServiceReportService: WinterServiceReportService,
    private geometryService: GeometryService
  ) {
    super();
    this.source = new VectorSource({});
  }

  /**
   * @internal
   */
  push(...features: Feature<Geometry>[]): void {
    this.source.addFeatures(features);
  }

  /**
   * @internal
   */
  remove(feature: Feature<Geometry>): void {
    this.source.removeFeature(feature);
  }

  /**
   * @internal
   */
  find(id: string): Feature<Geometry> | null {
    return this.source.getFeatureById(id);
  }

  getSource(): VectorSource {
    return this.source as VectorSource;
  }

  getData(): Observable<boolean> {
    return zip(
      this.http.get<{ features: { properties: { id: number, code: string, libelle: string, shape: string } }[] }>(`@domain/@api/lines/poly?types=vh`),
      this.winterServiceReportService.getData()
    ).pipe(
      map(([response, dynResponse]) => {
        for (const feature of response.features) {
          const geometry = this.geometryService.getGeometry(feature.properties.shape);
          delete feature.properties.shape;

          const dynData: WinterServiceReportMeasure = dynResponse.data.mesures.find(mesure => mesure.code === feature.properties.code);

          const f = new Feature({
            geometry,
            ...feature.properties,
            ...dynData,
          });
          f.setId(feature.properties.code);
          this.push(f);
        }
        this.startUpdateInterval(); // Start interval after initial data fetch
        this.updateSubject.next(dynResponse);
        return true;
      }),
      catchError(err => {
        console.error('Error getting VH data :', err);
        return of(false);
      })
    );
  }

  update(): Observable<boolean> {
    return this.winterServiceReportService.getData().pipe(
      map(response => {
        this.source.getFeatures().forEach(feature => {
          const id = feature.getId();
          const updatedMesure = response.data.mesures.find(mesure => mesure.code === id);
          if (id && updatedMesure) {
            for (const key in updatedMesure) {
              feature.set(key, updatedMesure[key]);
            }
          }
        });
        this.source.changed();
        this.updateSubject.next(response);
        return true;
      }),
      catchError(err => {
        console.error('Error getting VH data :', err);
        return of(false);
      })
    );
  }

  private startUpdateInterval(): void {
    if (!this.updateInterval$) {
      this.updateInterval$ = interval(60000).pipe( // Set interval to 1 minute
        switchMap(() => this.update())
      );

      this.updateInterval$.subscribe(); // Start the subscription to keep updating every minute
    }
  }

  get update$(): Observable<WinterServiceReport | null> {
    return this.updateSubject.asObservable();
  }
}
