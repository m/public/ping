import { Inject, Injectable, Type } from '@angular/core';
import { POPUP_TYPE } from './popup.token';
import { PopUpType } from '@features/map/map.model';
import { MapService } from '@features/map/map.service';
import { Coordinate } from 'ol/coordinate';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  private componentTypePopUpp: { [key: string]: Type<PopUpType> } = {};

  dataPopUp: BehaviorSubject<{data : any, typeData : string, btnDescription : boolean}> = new BehaviorSubject(null);

  constructor(@Inject(POPUP_TYPE) private popupType: PopUpType[], private mapService : MapService) {

    this.popupType.forEach(component => {
			this.componentTypePopUpp[component.typePopUp] = component.constructor as Type<PopUpType>;     
		});

  }

  getComponent(type: string): Type<PopUpType> {
    return this.componentTypePopUpp[type] as Type<PopUpType> || null;
  }

  hidePopup(): void {
		this.mapService.mapComponent.hidePopup();
	}

	showPopUp(coordinate: Coordinate): void {
		this.mapService.mapComponent.showPopUp(coordinate);
	}
}
