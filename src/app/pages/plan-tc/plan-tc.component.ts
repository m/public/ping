import { AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { CommonModule, NgFor } from '@angular/common';
import { PlanTCServiceSource } from '@features/map/plan-tc/planTC-service.source';
import { MapService } from '@features/map/map.service';
import { CoreModule, Line, LinesService, RoutesTypeService, RouteType } from '@metromobilite/m-features/core';
import { MapFiltersService } from '@metromobilite/m-map/m-layers';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { ReactiveFormsModule, UntypedFormBuilder } from '@angular/forms';
import { debounceTime, distinctUntilChanged, first, merge, Subject, switchMap, takeUntil, zip } from 'rxjs';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { Feature } from 'ol';
import {MatExpansionModule, MatExpansionPanel} from '@angular/material/expansion';
import {MatDividerModule} from '@angular/material/divider';
import { MapClickedFeature } from '@features/map/map.model';
import { PopupService } from '@features/components/popup-map/popup.service';
import { intersects } from 'ol/extent';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ClusterDecorationComponent } from '@features/components/cluster-decoration/cluster-decoration.component';
import { ApiMService } from '@services/api-m.service';
import { RouteService } from '@services/route.service';
import { AppComponent } from 'src/app/app.component';
import { GlobalService } from '@services/global.service';
import { PlanTCService } from './plan-tc.service';
import { LogoLigneComponent } from '@features/components/logo-ligne/logo-ligne.component';
import { ConfigurationComponent } from "../configuration/configuration.component";


@Component({
  selector: 'app-plan-tc',
  standalone: true,
  imports: [CommonModule, CoreModule, NgFor, MatCardModule, ReactiveFormsModule, MatSlideToggleModule, RouterLink, MatListModule, MatIconModule,
    MatExpansionModule, MatDividerModule, RouterModule, FormsModule, MatInputModule, MatButtonModule, ClusterDecorationComponent, LogoLigneComponent, ConfigurationComponent],
  templateUrl: './plan-tc.component.html',
  styleUrls: ['./plan-tc.component.scss']
})
export class PlanTCComponent implements OnInit, AfterViewInit, OnDestroy {


  
	hoveredFeature: Feature | null = null;
  results: any[] = [];
  private searchSubject: Subject<any> = new Subject();
  private unsubscriber: Subject<any> = new Subject();
  @ViewChildren(MatExpansionPanel) panels: QueryList<MatExpansionPanel>
  displayedResults: any[] = [];
  showAll: boolean = false;
  
  constructor(
    protected planTCServiceSource : PlanTCServiceSource, 
    private mapService: MapService,
    private linesService: LinesService,
		private mapFiltersService: MapFiltersService,
		private activatedRoute: ActivatedRoute,
		private fb: UntypedFormBuilder,
		private routesTypeService: RoutesTypeService,
    private apiMService : ApiMService,
    private popupService: PopupService,
    private routerActive: ActivatedRoute,
    private router: Router,
    private routeService: RouteService,
    private globalService: GlobalService,
    public planTCService : PlanTCService
  ) {
    this.searchSubject.pipe(
      takeUntil(this.unsubscriber),
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(term => term.length >= 3 ? this.apiMService.search(term) : [])
    ).subscribe(results => {
      
      this.results = results.features.filter(feature => {
        const id = feature.properties['code'];
        const agencyId = id.split(':')[0];

        return !['BUL', 'MCO', 'FUN', 'SNC'].includes(agencyId);
      });
      this.updateDisplayedResults();
      
    });
  }

  lines : Line[] = [];

  ngOnInit(): void {
    this.planTCService.routeTypes = this.routesTypeService.getRouteTypesInOrder(this.activatedRoute.snapshot.data.routesTypes, this.linesService.linesByType);

    zip(
      this.planTCServiceSource.getData(this.planTCService.routeTypes).pipe(takeUntil(this.unsubscriber)),
      this.planTCServiceSource.getClusters().pipe(takeUntil(this.unsubscriber)),
    )
    .subscribe((data) => {

      data[0].getFeatures().filter(f => f.getProperties().visible).forEach(feature => {
        let copy = {...feature.getProperties()};
        delete copy.visible;
        delete copy.geometry;
        this.lines.push((copy as Line));
      });

      this.mapService.showPlanTCServiceLayer();
      this.mapService.showClustersServiceLayer();

      this.routerActive.queryParams.pipe(first(), takeUntil(this.unsubscriber)).subscribe((event) => {         
                    
        this.planTCService.mapClustersIdByLineId = new Map();
          
        this.planTCService.disableLines();
        this.planTCService.disableClusters();

        if(typeof event.lignes !== 'undefined') {
          const lines : string[] = event.lignes.split(',');
          lines.forEach(element => {
            this.planTCService.selectLineById(element, true, false, false);
          }); 
        }

        if(typeof event.cluster !== 'undefined') {
          this.selectCluster(event.cluster);
        }
        
      });
    });

    // this.mapService.noticeMap$.next('Lorem ipsum dolor sit amet consectetur. Risus odio lobortis magna venenatis sed. Venenatis in malesuada tincidunt vestibulum vestibulum nisi nunc urna a. Lorem ipsum dolor sit amet consectetur. Risus odio lobortis magna venenatis sed. Venenatis in malesuada tincidunt vestibulum vestibulum nisi nunc urna a. Lorem ipsum dolor sit amet consectetur. Risus odio lobortis magna venenatis sed. Venenatis in malesuada tincidunt vestibulum vestibulum nisi nunc urna a. Lorem ipsum dolor sit amet consectetur. Risus odio lobortis magna venenatis sed. Venenatis in malesuada tincidunt vestibulum vestibulum nisi nunc urna a.');

    this.mapService.featureClicked$.pipe(takeUntil(this.unsubscriber)).subscribe((featureClicked: MapClickedFeature[] | null) => {    
      this.popupService.hidePopup();    //cacher la popup éventuellement ouverte
      this.popupService.dataPopUp.next(null); //supprimer le contenu éventuel de la popup

      this.planTCService.disableClusters(); 
      this.planTCService.disableLines(); 

      if(featureClicked == null || featureClicked.length == 0 || featureClicked[0].feature == null) { //si c'est un clickout alors
        this.planTCService.disableLines(); //désactiver toutes les lignes
        this.planTCService.mapClustersIdByLineId = new Map(); // réinitialiser la map

        this.router.navigate([], { //supprimer les queryParams de l'url
          relativeTo: this.routerActive,
          queryParams: { },
          replaceUrl: true
        });

        return;
      }


      let data : any = {lines: []};
      let typeData : string = '';
      let coordinate : any[] = [];
      let btnDescription : boolean = false;
      featureClicked.every(element => {
        coordinate = element.coordinate;
        if(element.feature.getProperties().type === 'clusters') {
          typeData = 'clusters';
          this.planTCService.mapClustersIdByLineId = new Map();
          btnDescription = true;
          data.cluster = element.feature.getProperties();
                    
          element.feature.set('visibleMap', true);
          element.feature.set('selected', true);
          //récupérer toutes les lignes qui passent à ce cluster
          this.apiMService.getRoutesForCluster(element.feature.getProperties().code).pipe(takeUntil(this.unsubscriber)).subscribe((routes) => {
            routes.forEach(oneRoute => { //pour chaque lignes alors
              data.lines.push(this.planTCService.selectLineById(oneRoute.id, true, false, true));//selectionner la ligne
            });

            this.popupService.dataPopUp.next({data, typeData, btnDescription});
            this.popupService.showPopUp(coordinate);
          });
          
          //ajouter l'id du cluster selectionné dans l'url
          this.router.navigate([], {
            relativeTo: this.routerActive,
            queryParams: { cluster: element.feature.getProperties().code },
            replaceUrl: true
          });

          //return false permet de stopper la boucle. Si on a un cluster, pas besoin de traiter les lignes séparrement en plus
          return false;
        }
        
        typeData = 'line';
        btnDescription = true;

        //Ici, on est sur une ligne selectionnée
        data.lines.push({feature : this.planTCService.selectLineById(element.feature.getProperties().id, true, true, true)});

        this.planTCService.setLignesInUrl();

        return true;
      });


      if(data.lines.length >= 0 && !data.cluster) {
        this.popupService.dataPopUp.next({data, typeData, btnDescription});
        this.popupService.showPopUp(coordinate);
      }
      
    });


    this.mapService.featureHovered$.pipe(takeUntil(this.unsubscriber)).subscribe((featureClicked: MapClickedFeature | null) => {
      if((featureClicked == null || featureClicked.feature == null) && this.hoveredFeature != null) {        
        this.hoveredFeature.set('hovered', false);
        this.hoveredFeature = null;
      }
      if(featureClicked != null && featureClicked.feature != null) {        
        if(this.hoveredFeature && featureClicked.feature.getProperties().type  === 'clusters' && this.hoveredFeature.getProperties().code == featureClicked.feature.getProperties().code) return;
        if(this.hoveredFeature && this.hoveredFeature.getProperties().id == featureClicked.feature.getProperties().id) return; 
        featureClicked.feature.set('hovered', true);
        this.hoveredFeature?.set('hovered', false);
        this.hoveredFeature = featureClicked.feature;
      }
    });

  }

  ngAfterViewInit(): void {
    this.planTCService.panels = this.panels;
  }

  selectCluster(codeCluster) {
    const feature = this.planTCServiceSource.sourcePOI.getFeatureById(codeCluster);
          
    if(typeof feature === 'undefined' || feature === null) return;
    
    feature.set('visibleMap', true);
    feature.set('selected', true);
    this.apiMService.getRoutesForCluster(feature.getProperties().code).pipe(first(), takeUntil(this.unsubscriber)).subscribe((routes) => {  
      let listFeature : Feature[] = [];                     
      routes.forEach(oneRoute => {
        listFeature.push(this.planTCService.selectLineById(oneRoute.id, true, false, true));
      });
    });

    this.goCluster(codeCluster);
    this.clearSearch();
}

  /**
   * Checks the selection status of a line on the map based on its unique identifier.
   *
   * @param line - The line object containing the unique identifier of the line to be checked.
   *
   * @returns {boolean} - A boolean value indicating whether the line is selected (true) or not (false).
   * If the line is not found in the map's data source, the function returns false.
   */
  selected(line: Line){   
    return this.planTCService.isSelected(line);
  }

  trackByFn(index: number, group: RouteType) {
		return group.type;
	}

	trackByFn2(index: number, line: Line) {
		return line.id;
	}

  trackByFn3(index: any, item: any) {
    return item.key;
  }


  clearSearch() {
    this.planTCService.searchTerm = '';
    this.results = this.displayedResults = [];
  }

  onSearch(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.searchSubject.next(target.value);
  }

  showMore(): void {
    this.showAll = true;
    this.updateDisplayedResults();
  }

  showLess(): void {
    this.showAll = false;
    this.updateDisplayedResults();
  }

  updateDisplayedResults(): void {
    if (this.showAll) {
      this.displayedResults = this.results;
    } else {
      this.displayedResults = this.results.slice(0, 3);
    }
  }

  goCluster(param: string){    
    this.clearSearch();
    this.router.navigate([], {
      relativeTo: this.routerActive,
      queryParams: {cluster: param},
      replaceUrl: true
    });
  }

  ngOnDestroy(): void {
    this.mapService.hidePlanTCServiceLayer();
    this.mapService.hideClustersServiceLayer();
    this.popupService.hidePopup();
    this.unsubscriber.next(true);
    this.mapService.noticeMap$.next('')
  }
}
