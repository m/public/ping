import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { BottomSheetComponent } from '@features/components/bottom-sheet/bottom-sheet.component';
import { ConfigService, FRONT_CONFIG, PingTile } from '@metromobilite/m-features/core';
import { ConfigPingService } from '@services/config.service';
import { GlobalService } from '@services/global.service';
import { CurrentRouteStaticData, RouteService } from '@services/route.service';
import { combineLatest, Subject } from 'rxjs';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {


  isHome: boolean = true;
  drawerOpened: boolean = true;
  title: string = 'Plan interactif';
  visible: boolean = false; //permet de savoir si le panel est ouvert ou fermé
  isMobile: boolean = false;
  isActionPanel: boolean = false; //permet de savoir si l'usager a modifié la map depuis le panel. Pour mettre le bouton en primary

  configuration: boolean = false; //permet de mettre le panel d'édition de la configuration

  labelPanel: string = 'Personnaliser la carte';
  mainMenu : any = {
    "id": "appliM",
    "header": {
      "principalMenu": {
        "children": [
        ]
      }
    }
  }
  configTiles: PingTile[] = [];
  environment = environment;

  unsubsciber: Subject<any> = new Subject();

  constructor(public routeService: RouteService, private router: Router, private activatedRoute: ActivatedRoute, private location: Location,
    public configPingService: ConfigPingService, private _bottomSheet: MatBottomSheet, public globalService: GlobalService) {

  }

  ngOnInit(): void {

    combineLatest(
      [this.configPingService.isConfigOk.pipe(takeUntil(this.unsubsciber)),
      this.configPingService.finalConfigObj.pipe(takeUntil(this.unsubsciber))]
    ).subscribe(([isOk, conf]) => {
      if(!isOk) return;
      this.configTiles = conf.tiles;
      this.prepareMenu();

      const currentTile = this.configTiles.find((tile: PingTile) => tile.slug === this.routeService.currentRouteStaticData.slug);
      let title = 'Plan Interfactif et Numérique de Grenoble';
      if (currentTile) title = currentTile.title;
      this.title = title;

      if(currentTile && currentTile.labelPanel) this.labelPanel = currentTile.labelPanel;
    });
        
    this.globalService.actionPanel.subscribe(action => {
      this.isActionPanel = action;     
    });

    this.routeService.currentRouteStaticData$.subscribe((routeData) => {
      this.isHome = routeData.data?.isHome;
    });

    this.activatedRoute.queryParamMap.subscribe((queryParams) => {
      const openSidenav = queryParams.get('openSidenav');
      this.drawerOpened = openSidenav ? openSidenav === 'true' : true;

      const configuration = queryParams.get('config');
      this.configuration = configuration ? configuration === 'true' : false;
    });

    // this.routeService.currentRouteStaticData$.subscribe((data: CurrentRouteStaticData) => {
    //   const currentTile = this.configTiles.find((tile: PingTile) => tile.slug === data.slug);
    //   let title = 'Plan Interfactif et Numérique de Grenoble';
    //   if (currentTile) title = currentTile.title;
    //   this.title = title;

    //   if(currentTile && currentTile.labelPanel) this.labelPanel = currentTile.labelPanel;
    // });

    this.globalService.isMobile.subscribe(isMobileState => {
      this.isMobile = isMobileState.matches;
    });
  }

  ngAfterViewInit() {
  }


  goBack() {
    this.location.back();
  }

  prepareMenu() {
    let tmpMenuString = JSON.stringify(this.mainMenu);
    let tmpMenu = JSON.parse(tmpMenuString);

    tmpMenu.header.principalMenu.children = this.configTiles.map(config => {return {
      type: "link",
      "label": config.title,
      "href": config.slug,
      "external": config.external || false
    }});

    this.mainMenu = JSON.parse(JSON.stringify(tmpMenu));
  }

  openBottomSheet(): void {
    this._bottomSheet.open(BottomSheetComponent, {
      data: { content: 'router-outlet' }
    });
  }

  disableActionPanel() {
    this.globalService.actionPanel.next(false);
  }

  ngOnDestroy(): void {   
      this.unsubsciber.next('finish');
      this.unsubsciber.complete();
  }
}
