import { Directive, ElementRef, OnDestroy, OnInit, Renderer2, ViewContainerRef } from '@angular/core';
import { PopupService } from './popup.service';
import { Subject, takeUntil } from 'rxjs';

@Directive({
  selector: '[appPopupMap]',
  standalone: true
})
export class PopupMapDirective implements OnInit, OnDestroy{

  private unsubscriber: Subject<any> = new Subject();
  constructor(public viewContainerRef: ViewContainerRef, private popupService : PopupService) { }

  ngOnInit(): void {

    this.popupService.dataPopUp.pipe(takeUntil(this.unsubscriber)).subscribe(
      data => {
        this.viewContainerRef.clear();

        if(data == null) return;
        
        const ref = this.viewContainerRef.createComponent(this.popupService.getComponent(data.typeData)); 
        ref.instance.data = data
      }
    )
    

  }

  ngOnDestroy(): void {
    this.unsubscriber.next('finish');
    this.unsubscriber.complete();
  }



}
