import { Injectable } from '@angular/core';
import { LayerInputConfig, MapComponent, MapLayer, MapLayerBase } from '@metromobilite/m-map';
import VectorLayer from 'ol/layer/Vector';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import { ZFESource } from './zfe.source';
import Base from 'ol/layer/Base';
import { getLayerStyle } from './layer-style.utils';
import { Feature } from 'ol';
import { MlayersCircleStyle, MlayersLineStyle, MlayersPlygonStyle } from '@metromobilite/m-map/m-layers';

@Injectable({ providedIn: 'root' })
@MapLayer('m-ping:zfe-heavy-vehicles')
export class ZfeHeavyVehiclesLayer extends MapLayerBase {
    constructor(public override source: ZFESource, private lineStyle: MlayersLineStyle, private polygonsStyle: MlayersPlygonStyle ) { super(); }

    buildLayer(component: MapComponent, config: LayerInputConfig): Base {
        return new VectorLayer({
            source: this.source.source,
            style: (feature, resolution) => getLayerStyle(feature as Feature, component, 'heavy', this.lineStyle, this.polygonsStyle)
        });
    }

    updateVisibility(date: Date) {
        this.source.filterFeaturesByPeriod(date, 'heavy');
    }
}