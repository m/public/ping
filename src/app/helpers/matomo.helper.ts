let _matomoConfig = {
    scriptUrl: 'https://www.mobilites-m.fr/stats/piwik.js',
    trackerUrl: 'https://www.mobilites-m.fr/stats/piwik.php',
    id: 25
};

if (['ping.mobilites-m.fr'].includes(window.location.hostname)) {
	_matomoConfig.id = 26;
}


export const matomoConfig = _matomoConfig;