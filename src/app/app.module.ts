import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterOutlet } from '@angular/router';
import { MapWrapperComponent } from '@features/map/map-wrapper/map-wrapper.component';
import { MFeaturesModule } from '@metromobilite/m-features';
import { ConfigService, CoreModule, FRONT_CONFIG } from '@metromobilite/m-features/core';
import { MLayersModule } from '@metromobilite/m-map/m-layers';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { domain } from './helpers/domain.helper';
import { HomeComponent } from "./pages/home/home.component";
import { AppMapModule } from '@features/map/map.module';
import { matomoConfig } from '@helpers/matomo.helper';
import { MatomoModule } from 'ngx-matomo';
import { PopupMapComponent } from '@features/components/popup-map/popup-map.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ThemeToggleComponent } from '@features/components/theme-toggle/theme-toggle.component';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { BottomSheetComponent } from '@features/components/bottom-sheet/bottom-sheet.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ConfigurationComponent } from "./pages/configuration/configuration.component";

export function initializeFrontConfigService(configService: ConfigService) {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      console.info('initializeFrontConfigService - started');
      configService.getData(FRONT_CONFIG.PING).subscribe({
        next: (config) => {
          console.info('initializeFrontConfigService - completed');
          resolve(config);
        },
        error: (err) => reject(err)
      });
    });
  };
}

@NgModule({
  schemas : [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    DragDropModule,
    MatomoModule.forRoot({
        scriptUrl: matomoConfig.scriptUrl,
        trackers: [
            {
                trackerUrl: matomoConfig.trackerUrl,
                siteId: matomoConfig.id
            }
        ],
        routeTracking: {
            enable: false
        }
    }),
    MFeaturesModule.forRoot({
        domain
        // domain: 'http://localhost:5400',
    }),
    MLayersModule,
    AppRoutingModule, RouterOutlet, CommonModule, MapWrapperComponent, PopupMapComponent, HomeComponent,
    AppMapModule,
    MatSidenavModule,
    MatButtonModule, MatIconModule, MatBottomSheetModule,
    ThemeToggleComponent, BottomSheetComponent,
    ConfigurationComponent
],
  providers: [{ provide: APP_INITIALIZER, useFactory: initializeFrontConfigService, deps: [ConfigService], multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule { }
