import { Pipe, PipeTransform } from '@angular/core';
import { domain } from '@helpers/domain.helper';
import { FRONT_CONFIG } from '@metromobilite/m-features/core';
import { ThemeToggleService } from '@services/theme-toggle.service';

@Pipe({
    name: 'thumbnail',
    standalone: true
})
export class ThumbnailPipe implements PipeTransform {

    constructor(private themeService : ThemeToggleService) {

    }
    transform(value: any): any {
        return `${domain}/api/front/config/${FRONT_CONFIG.PING}/file/${value}`;
    }
}