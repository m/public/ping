import { Injectable } from '@angular/core';
import { MapLayer, MapLayerBase, MapComponent, LayerInputConfig } from '@metromobilite/m-map';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { Fill, Stroke, Style } from 'ol/style';
import CircleStyle from 'ol/style/Circle';
import { ConfigService, FRONT_CONFIG } from '@metromobilite/m-features/core';
import { TemporaryMapSource } from './temporary-maps.source';
import { PingConfig, PingStyleConfig } from '../map.model';
import { ConfigPingService } from '@services/config.service';
import { MlayersCircleStyle, MlayersLineStyle, MlayersPlygonStyle, MlayersTextStyle } from '@metromobilite/m-map/m-layers';

@Injectable({ providedIn: 'root' })
@MapLayer('m-ping:temporary-map')
export class TemporaryMapLayer extends MapLayerBase {
  constructor(
    public source: TemporaryMapSource,
    private configPingService: ConfigPingService,
    private lineStyle: MlayersLineStyle,
    private textStyle: MlayersTextStyle,
    private circleStyle: MlayersCircleStyle,
    private polygonsStyle: MlayersPlygonStyle
  ) {
    super();
  }

  buildLayer(component: MapComponent, config: string | LayerInputConfig): BaseLayer {

    const layer = new VectorLayer({
      source: this.source.getSource() as VectorSource,
      style: (feature, resolution) => {

        const pingConfig: PingConfig = this.configPingService.finalConfig;
        
        const slug = feature.get('slug');
        const dataType = feature.get('dataType');

        if (!slug || !dataType) {
          return null; // Feature doesn't have the required metadata
        }

        const temporaryMap = pingConfig.temporaryMaps[slug];
        if (!temporaryMap) {
          return null; // No config found for this slug
        }

        const dataTypeConfig = temporaryMap.dataTypes.find((dt) => dt.api.includes(dataType));
        if (!dataTypeConfig) {
          return null; // No matching dataType found
        }

        const useDark = component.tiles.some((config) => config.visible && config.layer.includes('dark'));

        return this.buildStyle(feature, useDark, dataTypeConfig.style);
      },
    });

    return layer;
  }

  private buildStyle(feature: any, useDark: boolean, styleConfig: PingStyleConfig): Style | Style[] {
    const geometryType = feature.getGeometry().getType();
    const styleProperties = useDark ? styleConfig.dark : styleConfig.light;

    const color = styleProperties.color;
    const strokeColor = styleProperties.strokeColor || color;
    const strokeWidth = styleProperties.strokeWidth || 2;
    const borderWidth = styleProperties.borderWidth || 0; // Optional border width
    const borderStrokeColor = styleProperties.borderStrokeColor || '#000000'; // Optional border color
    const radius = styleProperties.radius || 6;
    const lineDash = styleProperties.lineDash || null;
    const zIndex = styleProperties.zIndex || 1;    
    const alternativeColor = styleProperties.alternativeColor || null;

    switch (geometryType) {
      case 'Point':
        // return new Style({
        //   image: new CircleStyle({
        //     radius,
        //     fill: new Fill({ color }),
        //     stroke: new Stroke({ color: strokeColor, width: strokeWidth }),
        //   }),
        //   zIndex
        // });
        return this.circleStyle.build({
          radius,
          color,
          strokeColor, 
          strokeWidth,
          zIndex
        })

      case 'LineString':
      case 'MultiLineString': {
        let styles = [];
        if (borderWidth > 0) {
          
          const tmpStyle = this.lineStyle.build({
            color: borderStrokeColor,
            width: strokeWidth + borderWidth,
            lineDash: lineDash,
            zOffset: zIndex
          })
          if (Array.isArray(tmpStyle)) {
            tmpStyle.forEach((style) => {
              styles.push(style);
            });
          } else {
            styles.push(tmpStyle);
          }
        }

        // Add a border style if borderWidth is defined
        const tmpStyle = this.lineStyle.build({
          color,
          width: strokeWidth,
          lineDash: lineDash,
          zOffset: zIndex
        })
        if (Array.isArray(tmpStyle)) {
          tmpStyle.forEach((style) => {
            styles.push(style);
          });
        } else {
          styles.push(tmpStyle);
        }
        return styles; // Return both styles as an array
      }

      case 'Polygon':
      case 'MultiPolygon':
        // return new Style({
        //   fill: new Fill({
        //     color,
        //   }),
        //   stroke: new Stroke({
        //     color: strokeColor,
        //     width: strokeWidth,
        //     lineDash,
        //     lineCap: 'round',
        //   }),
        //   zIndex
        // });
        let styles = [];
        const tmpStyle = this.polygonsStyle.build({
          color,
          alternateColor: alternativeColor,
          width: strokeWidth,
          lineDash: lineDash,
          zOffset: zIndex
        })
        if (Array.isArray(tmpStyle)) {
          tmpStyle.forEach((style) => {
            styles.push(style);
          });
        } else {
          styles.push(tmpStyle);
        }
        return styles

      default:
        console.warn(`Unsupported geometry type: ${geometryType}`);
        return null;
    }
  }

}
