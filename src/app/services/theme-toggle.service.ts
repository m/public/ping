import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { MapService } from '@features/map/map.service';
import { THEMES } from '@metromobilite/m-features/core';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ThemeToggleService {
  strokeColorCluster: string;
  fillColorCluster: string;
  public static readonly LS_KEY: string = 'm-ping:theme';

  theme: THEMES;
  theme$: Subject<THEMES> = new Subject<THEMES>();
  constructor(@Inject(DOCUMENT) private _document: any, private mapService: MapService) {
    let _theme: string = THEMES.LIGHT;
    try {
      _theme = localStorage.getItem(ThemeToggleService.LS_KEY);
      if (localStorage.getItem(ThemeToggleService.LS_KEY) === null) {
        localStorage.setItem(ThemeToggleService.LS_KEY, THEMES.DARK);
        _theme = localStorage.getItem(ThemeToggleService.LS_KEY);
      }
      this.theme = _theme as THEMES;
      this.setClasses();
    } catch (e) {
      console.error(e)
    }
  }

  public toggleTheme(theme: THEMES): void {
    this.theme = theme;
    this.setStorage();
    this.setClasses();
    this.setMapTiles();
  }

  public setClasses(): void {
    if (this.theme === THEMES.LIGHT) {
      this._document.body.classList.remove(THEMES.DARK);
      this._document.body.classList.add(THEMES.LIGHT);
      this.strokeColorCluster = '#000000';
      this.fillColorCluster = "#FFFFFF";
    } else if (this.theme === THEMES.DARK) {
      this._document.body.classList.remove(THEMES.LIGHT);
      this._document.body.classList.add(THEMES.DARK);
      this.strokeColorCluster = "#FFFFFF";
      this.fillColorCluster = '#000000';
    }
    this.theme$.next(this.theme);
  }

  private setStorage(): void {
    localStorage.setItem(ThemeToggleService.LS_KEY, this.theme);
  }

  private setMapTiles() {
    this.mapService.mapComponent.theme = this.theme;
  }

}
