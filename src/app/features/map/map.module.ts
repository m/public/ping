import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MAP_LAYER, MMapModule } from '@metromobilite/m-map';
import { MLayersModule } from '@metromobilite/m-map/m-layers';
import { WinterServiceReportLayer } from './winter-service/winter-service.layer';
import { PlanTCServiceLayer } from './plan-tc/planTC-service.layer';
import { ZfePrivateVehiclesLayer } from './zfe/zfe.vp.layer';
import { ZfeHeavyVehiclesLayer } from './zfe/zfe.pl.layer';
import { ClusterServiceLayer } from './plan-tc/clusters-service.layer';
import { POPUP_TYPE } from '@features/components/popup-map/popup.token';
import { PopupLineComponent } from '@features/components/popup-map/popup-line/popup-line.component';
import { PopupClusterComponent } from '@features/components/popup-map/popup-cluster/popup-cluster.component';
import { TemporaryMapLayer } from './temporary-maps/temporary-maps.layer';
import { PopupTemporaryMapComponent } from '@features/components/popup-map/popup-temporary-map/popup-temporary-map.component';

@NgModule({
    imports: [
        CommonModule,
        MMapModule,
        MLayersModule,
        MatIconModule,
        MatButtonModule,
        MatTooltipModule
    ],
    providers: [
        { provide: MAP_LAYER, useClass: WinterServiceReportLayer, multi: true },
        { provide: MAP_LAYER, useClass: ZfePrivateVehiclesLayer, multi: true },
        { provide: MAP_LAYER, useClass: ZfeHeavyVehiclesLayer, multi: true },
        { provide: MAP_LAYER, useClass: PlanTCServiceLayer, multi: true },
        { provide: MAP_LAYER, useClass: ClusterServiceLayer, multi: true },
        { provide: MAP_LAYER, useClass: TemporaryMapLayer, multi: true },
        { provide: POPUP_TYPE, useClass: PopupLineComponent, multi: true },
        { provide: POPUP_TYPE, useClass: PopupClusterComponent, multi: true },
        { provide: POPUP_TYPE, useClass: PopupTemporaryMapComponent, multi: true },
    ]
})

export class AppMapModule {
}
