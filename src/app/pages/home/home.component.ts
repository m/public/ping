import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { RouteService } from '@services/route.service';
import { ThumbnailPipe } from "../../pipes/thumbnail.pipe";
import { domain } from '@helpers/domain.helper';
import { PingTile } from '@metromobilite/m-features/core';
import { TileSlugPipe } from "../../pipes/tile-slug.pipe";
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, RouterModule, MatDividerModule, MatIconModule, MatButtonModule, MatCardModule, ThumbnailPipe, TileSlugPipe],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  schemas : [CUSTOM_ELEMENTS_SCHEMA]
})
export class HomeComponent implements OnInit {

  tiles: PingTile[] = [];
  domain = domain;
  environment = environment;

  get isGrid(): boolean {
    return this.tiles.filter(tile => tile.production).length > 2;
  }

  constructor(private appRouteService: RouteService) { }

  ngOnInit() {
    this.tiles = this.appRouteService.currentRouteStaticData.data.pingConfig.tiles;
  }
}
