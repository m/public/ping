/**
 * Utility function to darken a given color by a certain percentage.
 * @param color - The original color in hex format (e.g., #FF6600)
 * @param percent - The percentage by which to darken the color (0 to -100)
 * @returns The darkened color in hex format.
 */
export function darkenColor(color: string, percent: number): string {
    const num = parseInt(color.slice(1), 16);
    const amt = -(Math.round(2.55 * percent));
    const R = (num >> 16) + amt;
    const G = (num >> 8 & 0x00FF) + amt;
    const B = (num & 0x0000FF) + amt;
    return `#${(
      0x1000000 +
      (R < 255 ? (R < 1 ? 0 : R) : 255) * 0x10000 +
      (G < 255 ? (G < 1 ? 0 : G) : 255) * 0x100 +
      (B < 255 ? (B < 1 ? 0 : B) : 255)
    )
      .toString(16)
      .slice(1)
      .toUpperCase()}`;
  }