import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';

@Component({
  selector: 'app-card-ghost',
  standalone: true,
  imports: [CommonModule, MatCardModule, MatDividerModule],
  templateUrl: './card-ghost.component.html',
  styleUrls: ['./card-ghost.component.scss']
})
export class CardGhostComponent {

  @Input() rowCount: number = 3; // Default to 3 lines
  @Input() showCardHeader: boolean = true; // Whether to show the mat-card header
  @Input() showCardActions: boolean = false; // Whether to show the mat-card actions


  get rows() {
    return new Array(this.rowCount);
  }
}
