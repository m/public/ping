import { Pipe, PipeTransform } from '@angular/core';
import { PingTile } from '@metromobilite/m-features/core';

@Pipe({
  name: 'tileSlug',
  standalone: true
})
export class TileSlugPipe implements PipeTransform {

  transform(tile: PingTile): string {
    return tile.temporary ? `/carte-ephemere/${tile.slug}` : tile.slug;
  }
}
