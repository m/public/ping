import { ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FRONT_CONFIG } from '@metromobilite/m-features/core';
import { AbstractControl, FormArray, FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatTreeModule} from '@angular/material/tree';
import { MatInputModule } from '@angular/material/input';
import {MatExpansionModule} from '@angular/material/expansion';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDividerModule} from '@angular/material/divider';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { ObjectToArrayPipe } from '@pipes/object-to-array.pipe';
import { MatButtonModule } from '@angular/material/button';
import { JsonEditorOptions, NgJsonEditorModule } from 'ang-jsoneditor';
import { ConfigPingService } from '@services/config.service';
import { PlanTCServiceSource } from '@features/map/plan-tc/planTC-service.source';
import { MapService } from '@features/map/map.service';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { combineLatest, first, Subject, takeUntil } from 'rxjs';

export interface TreeNode {
  name: string;
  children?: TreeNode[];
  formControl?: FormControl; // Contrôle pour chaque nœud
}

const TREE_DATA: TreeNode[] = [];

@Component({
  selector: 'app-configuration',
  standalone: true,
  imports: [NgJsonEditorModule, CommonModule, FormsModule, ReactiveFormsModule, MatIconModule, MatTreeModule, MatFormFieldModule, MatTreeModule, MatInputModule, MatExpansionModule, ScrollingModule, MatTabsModule, MatDividerModule, MatCheckboxModule, MatButtonModule],
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigurationComponent implements OnInit, OnDestroy {

  Object = Object;

  @ViewChild('download', {read: ElementRef, static: true}) download!: ElementRef<HTMLAnchorElement>;

  editorOptions: JsonEditorOptions;
  // Données JSON initiales
  jsonData = {};
  
  configPing : any;
  editedData : string;
  unsubscriber: Subject<any> = new Subject();
  
  constructor(private configServicePing: ConfigPingService, private mapService: MapService) {
    this.editorOptions = new JsonEditorOptions();
    this.editorOptions.mode = 'tree';
    this.editorOptions.mainMenuBar = true;
  }


  ngOnInit(): void {
    combineLatest(
      [this.configServicePing.isConfigOk.pipe(takeUntil(this.unsubscriber)),
      this.configServicePing.finalConfigObj.pipe(takeUntil(this.unsubscriber))]
    ).pipe(takeUntil(this.unsubscriber)).subscribe(([isOk, conf]) => {      
      if(!isOk) return;
      this.configPing = {...conf};
      this.unsubscriber.next('ok');
      this.unsubscriber.complete();
    });
  }

  showJson(data) {
    this.editedData = JSON.stringify(data);
    this.configServicePing.setTemporaryConfig(JSON.parse(JSON.stringify(data)));
    this.mapService.reloadAllSources();
  }

  downloadConfiguration() {   
    this.download.nativeElement.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(this.editedData));
    this.download.nativeElement.setAttribute('download', 'configuration.json');
  }

  ngOnDestroy(): void {
    this.configServicePing.setTemporaryConfig({});
    this.unsubscriber.next('finish');
    this.unsubscriber.complete();
  }
}
