import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapInitGuard } from '@features/map/guards/map-init.guard';
import { LinesResolver, PingResolver, RoutesTypesResolver } from '@metromobilite/m-features/core';
import { ConfigurationComponent } from '@pages/configuration/configuration.component';
import { HomeComponent } from '@pages/home/home.component';
import { PlanTCComponent } from '@pages/plan-tc/plan-tc.component';
import { TemporaryMapComponent } from '@pages/temporary-map/temporary-map.component';
import { VhComponent } from '@pages/vh/vh.component';
import { ZfeComponent } from '@pages/zfe/zfe.component';

const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: '',
    component: HomeComponent,
    resolve: {
      pingConfig: PingResolver
    },
    data: {
      isHome: true
    }
  },
  {
    path: 'circuler-en-hiver',
    component: VhComponent,
    canActivate: [MapInitGuard],
    data: {
      title: 'Circuler en hiver',
      topLevel: true
    }
  },
  {
    path: 'zone-a-faibles-emissions',
    component: ZfeComponent,
    canActivate: [MapInitGuard],
    data: {
      title: 'Zones à Faibles Émissions (ZFE)',
      topLevel: true
    }
  },
  {
    path: 'plan-transport-commun',
    component: PlanTCComponent,
    resolve: { routes: LinesResolver, routesTypes: RoutesTypesResolver, pingConfig: PingResolver },
    canActivate: [MapInitGuard],
    data: {
      title: 'Plan transports en commun',
      topLevel: true
    }
  },
  {
    path: 'configuration',
    component: ConfigurationComponent,
    resolve: { routes: LinesResolver, routesTypes: RoutesTypesResolver, pingConfig: PingResolver },
    canActivate: [MapInitGuard],
    data: {
      title: 'Configuration',
      topLevel: true
    }
  },
  {
    path: ':slug',
    component: TemporaryMapComponent,
    canActivate: [MapInitGuard],
    data: {
      topLevel: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
